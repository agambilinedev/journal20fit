<?php
	$id_msg = isset($_GET['msgid']) ? $_GET['msgid'] : "";
	switch ($id_msg) {
		case 1:
			$msg_txt =  "All fields must not empty!";
			$msg_type = "error";
			break;
		case 2:
			$msg_txt =  "There was an error while inserting values, please try again later.";
			$msg_type = "error";
			break;
		case 3:
			$msg_txt =  "Floating Content Sucessfully added";
			$msg_type = "success";
			break;
		case 4:
			$msg_txt =  "Floating Content Sucessfully deleted";
			$msg_type = "success";
			break;
		case 5:
			$msg_txt =  "There was an error while deleting values, please try again later.";
			$msg_type = "error";
			break;
		case 6:
			$msg_txt =  "Floating Content Sucessfully updated";
			$msg_type = "success";
			break;
		case 7:
			$msg_txt =  "There was an error while updating values, please try again later.";
			$msg_type = "error";
			break;
		default:
			$msg_txt =  "";
			$msg_type = "";
	}
	if($msg_type=='error')
	{
		echo '<div class="error fade">
					<p><strong>'.$msg_txt.'</strong></p>
				</div> ';
	}
	else if($msg_type=='success')
	{
		echo '<div class="updated fade">
					<p><strong>'.$msg_txt.'</strong></p>
				</div> ';
	}
?>