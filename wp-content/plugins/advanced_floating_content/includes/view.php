<?php if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); } ?>
<?php
	global $wpdb;
	$table = $wpdb->prefix . 'afc';
	$table_inner = $wpdb->prefix . 'afc_impressions';
	$is_cpts=0;
	$where = "";
	$page = 0;
	$post = 0;
	$cat = 0;	
	if(is_page())
	{
		$pageid = get_the_ID();
		$where = "afc_cpts_pages = 1 or FIND_IN_SET($pageid,  `afc_slected_pages` ) <>0 and";	
		$page = 1;
	}
	if(is_single())
	{
		$postid = get_the_ID();
		$where = "afc_cpts_posts = 1 or FIND_IN_SET($postid,  `afc_cpts_posts` ) <>0 and";
		$post = 1;
	}
	if(is_category())
	{
		$cat_id = the_category_ID($echo = false);
		$where = "afc_cpts_category = 1 or FIND_IN_SET($cat_id,  `afc_cpts_category` ) <>0 and";
		$cat = 1;
	}
	if(is_home() || is_front_page())
	{
		$where = "afc_cpts_home = 1 and";
	}
	
	
	$builtin_cpts = array('post','page','attachment','revision','nav_menu_item');
	$post_type = get_post_type( get_the_ID() );
	if(!in_array($post_type, $builtin_cpts) && $post_type!="")
	{
		$is_cpts = 1;
		
	}
	if($is_cpts == 1)
	{
		
		$where = "afc_cpts_cpts = 1 or FIND_IN_SET('$post_type',  `afc_slected_cpts` ) <>0 and";
	}
	if($where=="") return;
	$sSql = "SELECT * FROM `".$table."` where ".$where." afc_status = 1 and 1=1 order by id_afc asc";
	$results = $wpdb->get_results($sSql, ARRAY_A);
	$classes="";
	$content = "";
	$responsive_view_320 = "";
	$responsive_view_480 = "";	
	foreach($results as $data)
	{
		
		if($post==1)
		{
			if($data['afc_cpts_posts']!=1)
			{
				$data_posts_ids = explode(",",$data['afc_slected_posts']);
				if(!in_array($postid,$data_posts_ids))
					return;
			}
		}
		if($page==1)
		{
			if($data['afc_cpts_pages']!=1)
			{
				$data_pages_ids = explode(",",$data['afc_slected_pages']);
				if(!in_array($pageid,$data_pages_ids))
					return;
			}
		}
		if($cat==1)
		{
			if($data['afc_cpts_category']!=1)
			{
				$data_cat_ids = explode(",",$data['afc_slected_category']);
				if(!in_array($cat_id,$data_cat_ids))
					return;
			}
		}
		$usql = "UPDATE `".$table."` SET afc_impression = afc_impression+1 WHERE id_afc = ".(int) $data['id_afc']." LIMIT 1";
		$wpdb->query($usql);
		
		$impressionresults = "";
		$user_ip = $_SERVER['REMOTE_ADDR'];
		$checkImpressionSql = "SELECT * FROM `".$table_inner."` where id_afc = '".(int) $data['id_afc']."' and user_ip = '".$user_ip."' and 1=1 limit 1";
		$impressionresults = $wpdb->get_results($checkImpressionSql);		
		if(isset($impressionresults[0]->id) && $impressionresults[0]->id != "")
		{
			
		}
		else{
			$classes .= "#afc_sidebar_".$data['id_afc']."{"."background:#".$data['afc_bg_color'].";";			
			if($data['afc_position_place']=="fixed") {
			$classes .="position:fixed;";
			}
			if($data['afc_position_place']=="absolute") {
			$classes .="position:absolute;";
			}			
			if($data['afc_position_y']=="top") {
			$classes .="top:0px;";
			}
			if($data['afc_position_y']=="bottom") {
			$classes .="bottom:0px;";
			}
			if($data['afc_position_x']=="left") {
			$classes .="left:0px;";
			}
			if($data['afc_position_x']=="right") {
			$classes .="right:0px;";
			}			
			$classes .="width:".$data['afc_width'].$data['afc_width_unit'].";";
			$classes .="margin-top:".$data['afc_margin_top']."px;";
			$classes .="margin-bottom:".$data['afc_margin_bottom']."px;";
			$classes .="margin-left:".$data['afc_margin_left']."px;";
			$classes .="margin-right:".$data['afc_margin_right']."px;";
			
			$px_border = explode(',',$data['afc_border_pixels']);
			$type_border = explode(',',$data['afc_border_type']);
			$color_border = explode(',',$data['afc_border_color']);
			$round_border = explode(',',$data['afc_border_rounded']);
			
			if($px_border[0]>0) {
				$classes .="border-top:".$px_border[0]."px ".$type_border[0]." #".$color_border[0].";";
				if($round_border[0]==1) {
					$classes .="border-top-left-radius:".$px_border[0]."px;";	
					$classes .="border-top-right-radius:".$px_border[0]."px;";
					$classes .="-moz-border-radius-topleft:".$px_border[0]."px;";
					$classes .="-moz-border-radius-topright:".$px_border[0]."px;";
				}
			}
			if($px_border[1]>0) {
				$classes .="border-bottom:".$px_border[1]."px ".$type_border[1]." #".$color_border[1].";";
				if($round_border[1]==1) {							
					$classes .="border-bottom-left-radius:".$px_border[1]."px;";	
					$classes .="border-bottom-right-radius:".$px_border[1]."px;";
					$classes .="-moz-border-radius-bottomleft:".$px_border[1]."px;";
					$classes .="-moz-border-radius-bottomright:".$px_border[1]."px;";
				}
			}
			if($px_border[2]>0) {
				$classes .="border-left:".$px_border[2]."px ".$type_border[2]." #".$color_border[2].";";
				if($round_border[2]==1) {
				
				}
			}
			if($px_border[3]>0) {
				$classes .="border-right:".$px_border[3]."px ".$type_border[3]." #".$color_border[3].";";
				if($round_border[3]==1) {
				
				}
			}
			$classes .="z-index:999999;";
			$classes .="padding:10px;";
			$classes .="color:#ffffff;";				
			$classes .="}"."\n";
			
			// for responsive view
			if($data['afc_control_devices']==1){
			
			$responsive_view_320 .= "#afc_sidebar_".$data['id_afc']."{";
			$responsive_view_320 .= "width:".(300-(($px_border[2]+$px_border[3])*2))."px !important";
			$responsive_view_320 .="}"."\n";
			
			$responsive_view_480 .= "#afc_sidebar_".$data['id_afc']."{";
			$responsive_view_480 .= "width:".(460-(($px_border[2]+$px_border[3])*2))."px !important";
			$responsive_view_480 .="}";
			
			}
			if($data['afc_control_devices']==2){
			
			$responsive_view_320 .= "#afc_sidebar_".$data['id_afc']."{";
			//$responsive_view_320 .= "display:none !important";
			$responsive_view_320 .="}"."\n";
			
			$responsive_view_480 .= "#afc_sidebar_".$data['id_afc']."{";
			//$responsive_view_480 .= "display:none !important";
			$responsive_view_480 .="}";
			
			}
			$classes .= $data['afc_css'];
						
			$content .= '<div id="afc_sidebar_'.$data['id_afc'].'" class="afc_popup">';
			if($data['afc_close']=="yes"){
					if($data['afc_impression_control']=="1"){$cdata = 'data="'.$_SERVER['REMOTE_ADDR'].','.$data['id_afc'].'"';} else {$cdata ="";}
				$content .='<a href="javascript:void()" class="afc_close_content" '.$cdata.'><img src="wp-content/plugins/advanced_floating_content/images/close.png" class="img" alt="" /></a>';
			}
			
			
			
			$content .= do_shortcode_output(stripslashes($data['afc_content']));
			$content .='</div>';
		}		
	}
	
	function do_shortcode_output($content) {
	  global $shortcode_tags;
	
	  if ( false === strpos( $content, '[' ) ) {
		return $content;
	  }
	
	  if (empty($shortcode_tags) || !is_array($shortcode_tags))
		return $content;
	
	  $pattern = get_shortcode_regex();
	  return preg_replace_callback( "/$pattern/s", 'do_shortcode_tag', $content );
	}
	
?>
<style type="text/css">
<?php echo $classes;?>
.afc_popup .img{position:absolute; top:-15px; right:-15px;}
@media screen and (min-width:481px) and (max-width:768px){
	/*.afc_popup{display:none;}*/
}
@media only screen and (min-width: 321px) and (max-width: 480px) {
<?php echo $responsive_view_480;?>
.afc_popup{
margin:0 !important;
}
.afc_popup iframe{width:100% !important;}
}
@media only screen and (max-width: 320px) {
<?php echo $responsive_view_320;?>
.afc_popup{
margin:0 !important;
}
.afc_popup iframe{width:100% !important;}
}
</style>
<?php 
	echo $content;
?>
<script type="text/javascript">
	(function ($) {
		$(".afc_close_content").click(function(){
			var attr = jQuery(this).attr('data');
			if (typeof attr !== typeof undefined && attr !== false) {				
				jQuery.post("<?php echo site_url(); ?>/wp-admin/admin-ajax.php", 
				{
					action:'controlImpressions',
					data:jQuery(this).attr('data')
				}, 
				function(jsontext){
							
				});
			}
			var afc_content_id = $(this).closest('div').attr('id');
			$("#"+afc_content_id).hide();
		});
	
		
	})(jQuery);

</script>
