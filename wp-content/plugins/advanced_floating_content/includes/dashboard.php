<?php if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); } ?>
<?php
	global $wpdb;
	$table = $wpdb->prefix . 'afc';
	$sSql = "SELECT * FROM `".$table."` where 1=1 order by id_afc desc";
	$results = $wpdb->get_results($sSql, ARRAY_A);
?>
<div class="afc_adm_body">
	<div class="afc_adm_content">
    	<div class="content-header">
            <div class="heading">Floating Content(s)</div>
            <div class="btn_new"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?page=afc_add_new">Add New</a></div>            
        </div>
        <div class="content-main">
        		<div class="col">
        	<div class="box">
            	<div class="box-header">
                    <h3 class="box-title"></h3>                                    
                </div>
                <?php
					include(WP_AFC_PATH_PLUGIN.'includes/messages.php');
				?>
                <div class="table box-body">
                	<table id="datatable" cellpadding="0" cellspacing="0" border="0" class="table-bordered table-striped tabledata" width="100%">
                        <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="35%">Title</th>
                                <th width="10%">Impressions</th>                            
                                <th width="10%">Status</th>
                                <th width="30%" class="last">Options</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
			if(count($results) > 0 ) {
				$key=0;
		 		foreach($results as $data) {
		?>
                        <tr bgcolor="<?php if($key%2==0) { echo "#f3f4f5"; } else {echo "#ffffff";}?>">
                            <td width="5%"><?php echo $data['id_afc']; ?></td>
                            <td width="35%"><?php echo stripslashes($data['afc_title']); ?></td>
                            <td width="10%"><?php echo $data['afc_impression']; ?></td>                            
                            <td width="10%"><?php if($data['afc_status']==1){ echo "Active";} else { echo "Inactive"; }?></td>
                            <td width="30%" class="last">
                            <a class="btn btn-app" href="<?php echo $_SERVER['PHP_SELF']; ?>?page=afc_add_new&action=edit&id=<?php echo $data['id_afc']?>"><i class="fa fa-edit"></i> Edit</a>
                            <a class="btn btn-app" href="<?php echo $_SERVER['PHP_SELF']; ?>?page=afc_add_new&action=delete&id=<?php echo $data['id_afc']?>" onclick="return confirm('Are you sure you want to delete?')"><i class="fa fa-trash-o"></i> Delete</a>
                            <a class="btn btn-app" href="<?php echo $_SERVER['PHP_SELF']; ?>?page=afc_add_new&action=view&id=<?php echo $data['id_afc']?>"><i class="fa fa-eye"></i> View</a>
                            </td>
                        </tr>
                        <?php $key++;} ?>
                        <?php }  else { ?>
        <tr><td colspan="5" align="center"><?php _e('No records available.', 'Advanced_Floating_contnet'); ?></td></tr>
		<?php } ?>
        			</tbody>
                     <tfoot>
                           
                        </tfoot>           
                    </table>
                	
                </div>
                <div class="afc_footer">
                	Plugin Developer : <a href="http://www.innovativeroots.com/" target="_blank">innovativeroots.com</a>
                </div>
            </div>
        </div>
        </div>
        
    </div>    
</div>
