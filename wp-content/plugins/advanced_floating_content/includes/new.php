<?php if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); } ?>
<?php
	global $wpdb,$data,$selected_posts,$selected_pages,$selected_category,$selected_cpts,$checked,$checked2,$checked3,$checked4,$checked5,$checked6,$checked7,$checked8,$checked9,$classes;
	$table = $wpdb->prefix . 'afc';
	$action = "add";
	$checked = 'checked="checked"';
	$checked3 = 'checked="checked"';
	$checked5 = 'checked="checked"';
	$column_data = "";
	$columns = "";
	
	if (isset($_POST['action']) && $_POST['action']=="add")
	{
		unset($_POST['id']);
		unset($_POST['action']);		
		unset($_POST['submit']);
		if($_POST['afc_cpts_posts']==1)
		{
			$_POST['afc_slected_posts'] = "";
		}
		if($_POST['afc_cpts_pages']==1)
		{
			$_POST['afc_slected_pages'] = "";
		}
		if($_POST['afc_cpts_category']==1)
		{
			$_POST['afc_slected_category'] = "";
		}
		if($_POST['afc_cpts_cpts']==1)
		{
			$_POST['afc_slected_cpts'] = "";
		}
		$afc_border_px = $_POST['afc_border_pixels_0'].",".$_POST['afc_border_pixels_1'].",".$_POST['afc_border_pixels_2'].",".$_POST['afc_border_pixels_3'];
		$afc_border_type = $_POST['afc_border_type_0'].",".$_POST['afc_border_type_1'].",".$_POST['afc_border_type_2'].",".$_POST['afc_border_type_3'];
		$afc_border_color = $_POST['afc_border_color_0'].",".$_POST['afc_border_color_1'].",".$_POST['afc_border_color_2'].",".$_POST['afc_border_color_3'];
		$afc_border_round = $_POST['afc_border_rounded_0'].",".$_POST['afc_border_rounded_1'].",".$_POST['afc_border_rounded_2'].",".$_POST['afc_border_rounded_3'];
		unset($_POST['afc_border_pixels_0']);
		unset($_POST['afc_border_pixels_1']);
		unset($_POST['afc_border_pixels_2']);
		unset($_POST['afc_border_pixels_3']);
		
		unset($_POST['afc_border_type_0']);
		unset($_POST['afc_border_type_1']);
		unset($_POST['afc_border_type_2']);
		unset($_POST['afc_border_type_3']);
		
		unset($_POST['afc_border_color_0']);
		unset($_POST['afc_border_color_1']);
		unset($_POST['afc_border_color_2']);
		unset($_POST['afc_border_color_3']);
		
		unset($_POST['afc_border_rounded_0']);
		unset($_POST['afc_border_rounded_1']);
		unset($_POST['afc_border_rounded_2']);
		unset($_POST['afc_border_rounded_3']);		
		
		$_POST['afc_content'] = addslashes($_POST['afc_content']);
		
		
		foreach($_POST as $key => $value)
		{
			if($value=="" && $key =="afc_title")
			{
				echo '<script type="text/javascript">window.location = "'.$_SERVER['PHP_SELF'].'?page=afc_floating_contents&msgid=1";</script>';
				exit;			
			}
			else
			{
				$column_data .= "'".$value."', ";
				$columns .= "`".$key."`, ";
			}
			
		}
		$columns .= "`afc_border_pixels`, ";
		$columns .= "`afc_border_type`, ";
		$columns .= "`afc_border_color`, ";
		$columns .= "`afc_border_rounded` ";
		
		$column_data .=  "'".$afc_border_px."', ";
		$column_data .=  "'".$afc_border_type."', ";
		$column_data .=  "'".$afc_border_color."', ";
		$column_data .=  "'".$afc_border_round."'";
			
		$sql = "INSERT INTO `".$table."` (".$columns.") VALUES(".$column_data.")";
		if($wpdb->query($sql))
		{
			echo '<script type="text/javascript">window.location = "'.$_SERVER['PHP_SELF'].'?page=afc_floating_contents&msgid=3";</script>';
			exit;	
		}
		else
		{
			echo '<script type="text/javascript">window.location = "'.$_SERVER['PHP_SELF'].'?page=afc_floating_contents&msgid=2";</script>';
			exit;
		}
	}
	
	if (isset($_GET['action']) && $_GET['action']=="delete")
	{
		$dsql = "DELETE FROM `".$table."` where id_afc = ".(int) $_GET['id'];				
		if($wpdb->query($dsql) === FALSE)
		{			
			echo '<script type="text/javascript">window.location = "'.$_SERVER['PHP_SELF'].'?page=afc_floating_contents&msgid=5";</script>';
			exit;
		}
		else
		{
			echo '<script type="text/javascript">window.location = "'.$_SERVER['PHP_SELF'].'?page=afc_floating_contents&msgid=4";</script>';
			exit;			
		}
	}
	
	if (isset($_POST['action']) && $_POST['action']=="update")
	{
		$id = $_POST['id'];
		unset($_POST['action']);
		unset($_POST['id']);		
		unset($_POST['submit']);
		if($_POST['afc_cpts_posts']==1)
		{
			$_POST['afc_slected_posts'] = "";
		}
		if($_POST['afc_cpts_pages']==1)
		{
			$_POST['afc_slected_pages'] = "";
		}
		if($_POST['afc_cpts_category']==1)
		{
			$_POST['afc_slected_category'] = "";
		}
		if($_POST['afc_cpts_cpts']==1)
		{
			$_POST['afc_slected_cpts'] = "";
		}
		$afc_border_px = $_POST['afc_border_pixels_0'].",".$_POST['afc_border_pixels_1'].",".$_POST['afc_border_pixels_2'].",".$_POST['afc_border_pixels_3'];
		$afc_border_type = $_POST['afc_border_type_0'].",".$_POST['afc_border_type_1'].",".$_POST['afc_border_type_2'].",".$_POST['afc_border_type_3'];
		$afc_border_color = $_POST['afc_border_color_0'].",".$_POST['afc_border_color_1'].",".$_POST['afc_border_color_2'].",".$_POST['afc_border_color_3'];
		$afc_border_round = $_POST['afc_border_rounded_0'].",".$_POST['afc_border_rounded_1'].",".$_POST['afc_border_rounded_2'].",".$_POST['afc_border_rounded_3'];
		unset($_POST['afc_border_pixels_0']);
		unset($_POST['afc_border_pixels_1']);
		unset($_POST['afc_border_pixels_2']);
		unset($_POST['afc_border_pixels_3']);
		
		unset($_POST['afc_border_type_0']);
		unset($_POST['afc_border_type_1']);
		unset($_POST['afc_border_type_2']);
		unset($_POST['afc_border_type_3']);
		
		unset($_POST['afc_border_color_0']);
		unset($_POST['afc_border_color_1']);
		unset($_POST['afc_border_color_2']);
		unset($_POST['afc_border_color_3']);
		
		unset($_POST['afc_border_rounded_0']);
		unset($_POST['afc_border_rounded_1']);
		unset($_POST['afc_border_rounded_2']);
		unset($_POST['afc_border_rounded_3']);
		$_POST['afc_content'] = addslashes($_POST['afc_content']);
		foreach($_POST as $key => $value)
		{
			if($value=="" && $key =="afc_title")
			{
				echo '<script type="text/javascript">window.location = "'.$_SERVER['PHP_SELF'].'?page=afc_floating_contents&msgid=1";</script>';
				exit;			
			}
			else
			{
				$column_data .=  "`".$key. "` = '".$value."', ";		
			}
		}
		$column_data .=  "`afc_border_pixels` = '".$afc_border_px."', ";
		$column_data .=  "`afc_border_type` = '".$afc_border_type."', ";
		$column_data .=  "`afc_border_color` = '".$afc_border_color."', ";
		$column_data .=  "`afc_border_rounded` = '".$afc_border_round."'";	
		$usql = "UPDATE `".$table."` SET ".$column_data." WHERE id_afc = ".(int) $id." LIMIT 1";
		if($wpdb->query($usql) === FALSE)
		{			
			echo '<script type="text/javascript">window.location = "'.$_SERVER['PHP_SELF'].'?page=afc_floating_contents&msgid=7";</script>';
			exit;
		}
		else
		{
			echo '<script type="text/javascript">window.location = "'.$_SERVER['PHP_SELF'].'?page=afc_floating_contents&msgid=6";</script>';
			exit;			
		}		
	}
	if (isset($_GET['action']) && $_GET['action']=="edit")
	{
		$qry = "SELECT * FROM `".$table."` where id_afc = ".(int) $_GET['id'];		
		$data = $wpdb->get_row($qry, ARRAY_A);
		
		
		if ($data['afc_status']==1) {
			$checked = 'checked="checked"';
			$checked2 = '';
		}
		else {
			$checked = '';
			$checked2 = 'checked="checked"';
		}
		if ($data['afc_impression_control']==1) {
			$checked3 = 'checked="checked"';
			$checked4 = '';
		}
		else {
			$checked3 = '';
			$checked4 = 'checked="checked"';
		}
		
		if ($data['afc_control_devices']==1) {
			$checked5 = 'checked="checked"';
			$checked6 = '';
			$checked7 = '';
		}
		else if($data['afc_control_devices']==2) {
			$checked5 = '';
			$checked6 = 'checked="checked"';
			$checked7 = '';
		}
		else{
			$checked5 = '';
			$checked6 = '';
			$checked7 = 'checked="checked"';
		}
		$action = "update";
	}
	if (isset($_GET['action']) && $_GET['action']=="view")
	{
		$qry = "SELECT * FROM `".$table."` where id_afc = ".(int) $_GET['id'];		
		$data = $wpdb->get_row($qry, ARRAY_A);
	}
	
	$postlist = get_posts( array('post_status'=>'publish','orderby'=>'post_title','posts_per_page'=> -1));
	$pagelist = get_pages( array('post_status'=>'publish','post_type' => 'page','sort_column' => 'post_title','posts_per_page'=> -1));
	$categorieslist = get_categories( array('orderby'=>'name','taxonomy'=>'category'));
	$args = array(
	   'public'   => true,
	   '_builtin' => false
	);
	$output = 'names'; // names or objects, note names is the default
	$operator = 'and'; // 'and' or 'or'
	$cptslist = get_post_types($args, $output, $operator );
	
	
	
	
	
	$data['afc_cpts_home'] = isset($data['afc_cpts_home']) ? $data['afc_cpts_home'] : '1';
	$data['afc_cpts_posts'] = isset($data['afc_cpts_posts']) ? $data['afc_cpts_posts'] : '1';
	$data['afc_cpts_pages'] = isset($data['afc_cpts_pages']) ? $data['afc_cpts_pages'] : '1';
	$data['afc_cpts_category'] = isset($data['afc_cpts_category']) ? $data['afc_cpts_category'] : '1';
	$data['afc_cpts_cpts'] = isset($data['afc_cpts_cpts']) ? $data['afc_cpts_cpts'] : '1';
	
	$data['afc_slected_posts'] = isset($data['afc_slected_posts']) ? $data['afc_slected_posts'] : '';
	$data['afc_slected_pages'] = isset($data['afc_slected_pages']) ? $data['afc_slected_pages'] : '';
	$data['afc_slected_category'] = isset($data['afc_slected_category']) ? $data['afc_slected_category'] : '';
	$data['afc_slected_cpts'] = isset($data['afc_slected_cpts']) ? $data['afc_slected_cpts'] : '';
	
	$selected_posts = explode(',',$data['afc_slected_posts']);
	$selected_pages = explode(',',$data['afc_slected_pages']);
	$selected_category = explode(',',$data['afc_slected_category']);
	$selected_cpts = explode(',',$data['afc_slected_cpts']);
	
	
	
		$data['afc_border_pixels'] = isset($data['afc_border_pixels']) ? $data['afc_border_pixels'] : '0,0,0,0';
		$data['afc_border_type'] = isset($data['afc_border_type']) ? $data['afc_border_type'] : 'solid,solid,solid,solid,';
		$data['afc_border_color'] = isset($data['afc_border_color']) ? $data['afc_border_color'] : 'cccccc,cccccc,cccccc,cccccc';		
		$data['afc_border_rounded'] = isset($data['afc_border_rounded']) ? $data['afc_border_rounded'] : '0,0,0,0';
		
		$border_px = explode(',',$data['afc_border_pixels']);
		$border_type = explode(',',$data['afc_border_type']);
		$border_color = explode(',',$data['afc_border_color']);
		$border_round = explode(',',$data['afc_border_rounded']);
	
	
	$data['afc_content'] = isset($data['afc_content']) ? stripslashes($data['afc_content']) : "";
	
?>
<style type="text/css">
	#afc_border_color_0{	
		border-right:20px solid #<?php echo $border_color[0]; ?>;
		line-height:16px;
	}
	#afc_border_color_1 {	
		border-right:20px solid #<?php echo $border_color[1]; ?>;
		line-height:16px;
	}
	#afc_border_color_2 {	
		border-right:20px solid #<?php echo $border_color[2]; ?>;
		line-height:16px;
	}
	#afc_border_color_3 {	
		border-right:20px solid #<?php echo $border_color[3]; ?>;
		line-height:16px;
	}
	<?php echo isset($data['afc_css']) ? $data['afc_css'] : ''; ?>
</style>
<div class="afc_adm_body">
	<div class="afc_adm_content">
    	<div class="content-header">
            <div class="heading">Floating Content(s)</div>
            <div class="btn_new"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?page=afc_add_new">Add New</a></div>            
        </div>
        <div class="content-main">
        		<div class="col">
           
            <div class="box">
             <?php if(isset($_GET['action']) && $_GET['action']=='view') { ?>
             <?php
             	$classes .="background:#".$data['afc_bg_color'].";";
			 	$classes .="width:".$data['afc_width'].$data['afc_width_unit'].";";		
				$classes .="border:".$data['afc_border_pixels']."px ".$data['afc_border_type']." #".$data['afc_border_color'].";";
				if($data['afc_border_rounded']==1) {
					$classes .="-moz-border-radius:".$data['afc_border_pixels']."px;";
					$classes .="border-radius:".$data['afc_border_pixels']."px;";
				}
				$classes .="padding:10px;";
				$classes .="color:#ffffff;";				
				$classes .="}";
				$classes .= $data['afc_css'];
			 
			 ?>
            	<div class="box-header">
                    <h3 class="box-title">View Content</h3>                                    
                </div>                
                <div class="view_content" style=" <?php echo $classes;?>">
                <?php echo stripslashes($data['afc_content']); ?>
                </div>
            
            <?php } else { ?>
            	<div class="box-header">
                    <h3 class="box-title">Add/Edit Content</h3>
                </div>                
                <div class="box box-primary">
                                <!-- form start -->
                                <form role="form" action="" name="afc_form_add_new" id="afc_form_add_new" method="post" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="Title">Title:</label>
                                            <input type="text" class="form-control" id="afc_title" name="afc_title" placeholder="Enter Title" value="<?php echo isset($data['afc_title']) ? stripslashes($data['afc_title']) : "";?>">
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="Content">Content:</label>
                                            <?php wp_editor($data['afc_content'],'afc_content'); ?>                                           
                                        </div>
                                        <div class="form-group">
                                        	<label for="Position">Position:</label>                                            
                                            <select name="afc_position_place" id="afc_position_place" class="form-control-10">
                                            	<option value="fixed" <?php if(isset($data['afc_position_place']) && $data['afc_position_place']=="fixed") { ?> selected="selected" <?php } ?>>Fixed</option>
                                                <option value="absolute" <?php if(isset($data['afc_position_place']) && $data['afc_position_place']=="absolute") { ?> selected="selected" <?php } ?>>Absolute</option>
                                            </select>
                                            <select name="afc_position_y" id="afc_position_y" class="form-control-10">
                                            	<option value="top" <?php if(isset($data['afc_position_y']) && $data['afc_position_y']=="top") { ?> selected="selected" <?php } ?>>Top</option>
                                                <option value="bottom" <?php if(isset($data['afc_position_y']) && $data['afc_position_y']=="bottom") { ?> selected="selected" <?php } ?>>Bottom</option>
                                            </select>
                                            <select name="afc_position_x" id="afc_position_x" class="form-control-10">
                                            	<option value="left" <?php if(isset($data['afc_position_x']) && $data['afc_position_x']=="left") { ?> selected="selected" <?php } ?>>Left</option>
                                                <option value="right" <?php if(isset($data['afc_position_x']) && $data['afc_position_x']=="right") { ?> selected="selected" <?php } ?>>Right</option>                                               
                                           </select>                                                 
                                        </div>
                                        <div class="form-group">
                                            <label for="Close">Show Close Button:</label>
											<select name="afc_close" id="afc_close" class="form-control-10">
                                            	<option value="yes" <?php if((isset($data['afc_close']) && $data['afc_close']=="yes") || (isset($data['afc_close']) && $data['afc_close']=="")) { ?> selected="selected" <?php } ?>>Yes</option>
                                                <option value="no" <?php if(isset($data['afc_close']) && $data['afc_close']=="no") { ?> selected="selected" <?php } ?>>No</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="Width">Width:</label>                           
<input type="text" class="form-control-10" id="afc_width" name="afc_width" placeholder="px" value="<?php echo isset($data['afc_width']) ? $data['afc_width'] : '100'; ?>"> 
                                            <select name="afc_width_unit" id="afc_width_unit" class="form-control-10">
                                                <option value="px" <?php if(isset($data['afc_width_unit']) && $data['afc_width_unit']=="px") { ?> selected="selected" <?php } ?>>inPixels</option>
                                                <option value="%" <?php if(isset($data['afc_width_unit']) && $data['afc_width_unit']=="%") { ?> selected="selected" <?php } ?>>inPercentage</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="Background">Background Color:</label>                                            
#<input type="text" class="form-control-10" id="afc_bg_color" name="afc_bg_color" placeholder="ffffff" value="<?php echo isset($data['afc_bg_color']) ? $data['afc_bg_color'] : 'ffffff'; ?>"> 
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="MarginTop">Margin Top:</label>
                                           	<input type="text" class="form-control-10" id="afc_margin_top" name="afc_margin_top" placeholder="px" value="<?php echo isset($data['afc_margin_top']) ? $data['afc_margin_top'] : '0'; ?>"> px                                            
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="MarginTop">Margin Bottom:</label>                                           
                                            <input type="text" class="form-control-10" id="afc_margin_bottom" name="afc_margin_bottom" placeholder="px" value="<?php echo isset($data['afc_margin_bottom']) ? $data['afc_margin_bottom'] : '0'; ?>"> px
                                        </div>
                                        <div class="form-group">
                                            <label for="MarginTop">Margin Left:</label>
                                             <input type="text" class="form-control-10" id="afc_margin_left" name="afc_margin_left" placeholder="px" value="<?php echo isset($data['afc_margin_left']) ? $data['afc_margin_left'] : '0'; ?>"> px
                                        </div>
                                        <div class="form-group">
                                            <label for="MarginTop">Margin Right:</label>
                                           	<input type="text" class="form-control-10" id="afc_margin_right" name="afc_margin_right" placeholder="px" value="<?php echo isset($data['afc_margin_right']) ? $data['afc_margin_right'] : '0'; ?>"> px
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="Border">Border Top:</label>
                                            <select name="afc_border_pixels_0" id="afc_border_pixels_0" class="form-control-10">
                                            	<option value="0" <?php if($border_px[0]==0) { ?> selected="selected" <?php } ?>>0px</option>
                                                <option value="1" <?php if($border_px[0]==1) { ?> selected="selected" <?php } ?>>1px</option>
                                                <option value="2" <?php if($border_px[0]==2) { ?> selected="selected" <?php } ?>>2px</option>
                                                <option value="3" <?php if($border_px[0]==3) { ?> selected="selected" <?php } ?>>3px</option>
                                                <option value="4" <?php if($border_px[0]==4) { ?> selected="selected" <?php } ?>>4px</option>
                                                <option value="5" <?php if($border_px[0]==5) { ?> selected="selected" <?php } ?>>5px</option>
                                                <option value="6" <?php if($border_px[0]==6) { ?> selected="selected" <?php } ?>>6px</option>
                                                <option value="7" <?php if($border_px[0]==7) { ?> selected="selected" <?php } ?>>7px</option>
                                           		<option value="8" <?php if($border_px[0]==8) { ?> selected="selected" <?php } ?>>8px</option>
                                           		<option value="9" <?php if($border_px[0]==9) { ?> selected="selected" <?php } ?>>9px</option>
                                                <option value="10" <?php if($border_px[0]==10) { ?> selected="selected" <?php } ?>>10px</option>
                                            </select>
                                            <select name="afc_border_type_0" id="afc_border_type_0" class="form-control-10">
                                                <option value="solid" <?php if($border_type[0]=="solid") { ?> selected="selected" <?php } ?>>Solid</option>
                                                <option value="medium" <?php if($border_type[0]=="medium") { ?> selected="selected" <?php } ?>>Medium</option>
                                                <option value="thin" <?php if($border_type[0]=="thin") { ?> selected="selected" <?php } ?>>Thin</option>
                                                <option value="thick" <?php if($border_type[0]=="thick") { ?> selected="selected" <?php } ?>>Thick</option>
                                                <option value="dashed" <?php if($border_type[0]=="dashed") { ?> selected="selected" <?php } ?>>Dashed</option>
                                                <option value="dotted" <?php if($border_type[0]=="dotted") { ?> selected="selected" <?php } ?>>Dotted</option>
                                                <option value="groove" <?php if($border_type[0]=="groove") { ?> selected="selected" <?php } ?>>Groove</option>
                                                <option value="inset" <?php if($border_type[0]=="inset") { ?> selected="selected" <?php } ?>>Inset</option>
                                                <option value="outset" <?php if($border_type[0]=="outset") { ?> selected="selected" <?php } ?>>Outset</option>
                                                <option value="ridge" <?php if($border_type[0]=="ridge") { ?> selected="selected" <?php } ?>>Ridge</option>
                                                <option value="none" <?php if($border_type[0]=="none") { ?> selected="selected" <?php } ?>>None</option>
 
                                            </select>
                                            #<input type="text" class="form-control-10" maxlength="6" name="afc_border_color_0" id="afc_border_color_0" value="<?php echo $border_color[0]; ?>" />


                                            <select name="afc_border_rounded_0" id="afc_border_rounded_0" class="form-control-10" style="width:150px;">
                                                <option value="0" <?php if($border_round[0]=="0") { ?> selected="selected" <?php } ?>>Straight Cornor</option>
                                                <option value="1" <?php if($border_round[0]=="1") { ?> selected="selected" <?php } ?>>Round Cornor</option>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="Border">Border Bottom:</label>
                                            <select name="afc_border_pixels_1" id="afc_border_pixels_1" class="form-control-10">
                                            	<option value="0" <?php if($border_px[1]==0) { ?> selected="selected" <?php } ?>>0px</option>
                                                <option value="1" <?php if($border_px[1]==1) { ?> selected="selected" <?php } ?>>1px</option>
                                                <option value="2" <?php if($border_px[1]==2) { ?> selected="selected" <?php } ?>>2px</option>
                                                <option value="3" <?php if($border_px[1]==3) { ?> selected="selected" <?php } ?>>3px</option>
                                                <option value="4" <?php if($border_px[1]==4) { ?> selected="selected" <?php } ?>>4px</option>
                                                <option value="5" <?php if($border_px[1]==5) { ?> selected="selected" <?php } ?>>5px</option>
                                                <option value="6" <?php if($border_px[1]==6) { ?> selected="selected" <?php } ?>>6px</option>
                                                <option value="7" <?php if($border_px[1]==7) { ?> selected="selected" <?php } ?>>7px</option>
                                           		<option value="8" <?php if($border_px[1]==8) { ?> selected="selected" <?php } ?>>8px</option>
                                           		<option value="9" <?php if($border_px[1]==9) { ?> selected="selected" <?php } ?>>9px</option>
                                                <option value="10" <?php if($border_px[1]==10) { ?> selected="selected" <?php } ?>>10px</option>
                                            </select>
                                            <select name="afc_border_type_1" id="afc_border_type_1" class="form-control-10">
                                                <option value="solid" <?php if($border_type[1]=="solid") { ?> selected="selected" <?php } ?>>Solid</option>
                                                <option value="medium" <?php if($border_type[1]=="medium") { ?> selected="selected" <?php } ?>>Medium</option>
                                                <option value="thin" <?php if($border_type[1]=="thin") { ?> selected="selected" <?php } ?>>Thin</option>
                                                <option value="thick" <?php if($border_type[1]=="thick") { ?> selected="selected" <?php } ?>>Thick</option>
                                                <option value="dashed" <?php if($border_type[1]=="dashed") { ?> selected="selected" <?php } ?>>Dashed</option>
                                                <option value="dotted" <?php if($border_type[1]=="dotted") { ?> selected="selected" <?php } ?>>Dotted</option>
                                                <option value="groove" <?php if($border_type[1]=="groove") { ?> selected="selected" <?php } ?>>Groove</option>
                                                <option value="inset" <?php if($border_type[1]=="inset") { ?> selected="selected" <?php } ?>>Inset</option>
                                                <option value="outset" <?php if($border_type[1]=="outset") { ?> selected="selected" <?php } ?>>Outset</option>
                                                <option value="ridge" <?php if($border_type[1]=="ridge") { ?> selected="selected" <?php } ?>>Ridge</option>
                                                <option value="none" <?php if($border_type[1]=="none") { ?> selected="selected" <?php } ?>>None</option>
                                            </select>
                                            #<input type="text" class="form-control-10" maxlength="6" name="afc_border_color_1" id="afc_border_color_1" value="<?php echo $border_color[1]; ?>" />


                                            <select name="afc_border_rounded_1" id="afc_border_rounded_1" class="form-control-10" style="width:150px;">
                                                <option value="0" <?php if($border_round[1]=="0") { ?> selected="selected" <?php } ?>>Straight Cornor</option>
                                                <option value="1" <?php if($border_round[1]=="1") { ?> selected="selected" <?php } ?>>Round Cornor</option>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="Border">Border Left:</label>
                                            <select name="afc_border_pixels_2" id="afc_border_pixels_2" class="form-control-10">
                                            	<option value="0" <?php if($border_px[2]==0) { ?> selected="selected" <?php } ?>>0px</option>
                                                <option value="1" <?php if($border_px[2]==1) { ?> selected="selected" <?php } ?>>1px</option>
                                                <option value="2" <?php if($border_px[2]==2) { ?> selected="selected" <?php } ?>>2px</option>
                                                <option value="3" <?php if($border_px[2]==3) { ?> selected="selected" <?php } ?>>3px</option>
                                                <option value="4" <?php if($border_px[2]==4) { ?> selected="selected" <?php } ?>>4px</option>
                                                <option value="5" <?php if($border_px[2]==5) { ?> selected="selected" <?php } ?>>5px</option>
                                                <option value="6" <?php if($border_px[2]==6) { ?> selected="selected" <?php } ?>>6px</option>
                                                <option value="7" <?php if($border_px[2]==7) { ?> selected="selected" <?php } ?>>7px</option>
                                           		<option value="8" <?php if($border_px[2]==8) { ?> selected="selected" <?php } ?>>8px</option>
                                           		<option value="9" <?php if($border_px[2]==9) { ?> selected="selected" <?php } ?>>9px</option>
                                                <option value="10" <?php if($border_px[2]==10) { ?> selected="selected" <?php } ?>>10px</option>
                                            </select>
                                            <select name="afc_border_type_2" id="afc_border_type_2" class="form-control-10">
                                                <option value="solid" <?php if($border_type[2]=="solid") { ?> selected="selected" <?php } ?>>Solid</option>
                                                <option value="medium" <?php if($border_type[2]=="medium") { ?> selected="selected" <?php } ?>>Medium</option>
                                                <option value="thin" <?php if($border_type[2]=="thin") { ?> selected="selected" <?php } ?>>Thin</option>
                                                <option value="thick" <?php if($border_type[2]=="thick") { ?> selected="selected" <?php } ?>>Thick</option>
                                                <option value="dashed" <?php if($border_type[2]=="dashed") { ?> selected="selected" <?php } ?>>Dashed</option>
                                                <option value="dotted" <?php if($border_type[2]=="dotted") { ?> selected="selected" <?php } ?>>Dotted</option>
                                                <option value="groove" <?php if($border_type[2]=="groove") { ?> selected="selected" <?php } ?>>Groove</option>
                                                <option value="inset" <?php if($border_type[2]=="inset") { ?> selected="selected" <?php } ?>>Inset</option>
                                                <option value="outset" <?php if($border_type[2]=="outset") { ?> selected="selected" <?php } ?>>Outset</option>
                                                <option value="ridge" <?php if($border_type[2]=="ridge") { ?> selected="selected" <?php } ?>>Ridge</option>
                                                <option value="none" <?php if($border_type[2]=="none") { ?> selected="selected" <?php } ?>>None</option>
 
                                            </select>
                                            #<input type="text" class="form-control-10" maxlength="6" name="afc_border_color_2" id="afc_border_color_2" value="<?php echo $border_color[2]; ?>" />


                                            <select name="afc_border_rounded_2" id="afc_border_rounded_2" class="form-control-10" style="width:150px;">
                                               <option value="0" <?php if($border_round[2]=="0") { ?> selected="selected" <?php } ?>>Straight Cornor</option>
                                                <option value="1" <?php if($border_round[2]=="1") { ?> selected="selected" <?php } ?>>Round Cornor</option>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="Border">Border Right:</label>
                                            <select name="afc_border_pixels_3" id="afc_border_pixels_3" class="form-control-10">
                                            	<option value="0" <?php if($border_px[3]==0) { ?> selected="selected" <?php } ?>>0px</option>
                                                <option value="1" <?php if($border_px[3]==1) { ?> selected="selected" <?php } ?>>1px</option>
                                                <option value="2" <?php if($border_px[3]==2) { ?> selected="selected" <?php } ?>>2px</option>
                                                <option value="3" <?php if($border_px[3]==3) { ?> selected="selected" <?php } ?>>3px</option>
                                                <option value="4" <?php if($border_px[3]==4) { ?> selected="selected" <?php } ?>>4px</option>
                                                <option value="5" <?php if($border_px[3]==5) { ?> selected="selected" <?php } ?>>5px</option>
                                                <option value="6" <?php if($border_px[3]==6) { ?> selected="selected" <?php } ?>>6px</option>
                                                <option value="7" <?php if($border_px[3]==7) { ?> selected="selected" <?php } ?>>7px</option>
                                           		<option value="8" <?php if($border_px[3]==8) { ?> selected="selected" <?php } ?>>8px</option>
                                           		<option value="9" <?php if($border_px[3]==9) { ?> selected="selected" <?php } ?>>9px</option>
                                                <option value="10" <?php if($border_px[3]==10) { ?> selected="selected" <?php } ?>>10px</option>
                                            </select>
                                            <select name="afc_border_type_3" id="afc_border_type_3" class="form-control-10">
                                                <option value="solid" <?php if($border_type[3]=="solid") { ?> selected="selected" <?php } ?>>Solid</option>
                                                <option value="medium" <?php if($border_type[3]=="medium") { ?> selected="selected" <?php } ?>>Medium</option>
                                                <option value="thin" <?php if($border_type[3]=="thin") { ?> selected="selected" <?php } ?>>Thin</option>
                                                <option value="thick" <?php if($border_type[3]=="thick") { ?> selected="selected" <?php } ?>>Thick</option>
                                                <option value="dashed" <?php if($border_type[3]=="dashed") { ?> selected="selected" <?php } ?>>Dashed</option>
                                                <option value="dotted" <?php if($border_type[3]=="dotted") { ?> selected="selected" <?php } ?>>Dotted</option>
                                                <option value="groove" <?php if($border_type[3]=="groove") { ?> selected="selected" <?php } ?>>Groove</option>
                                                <option value="inset" <?php if($border_type[3]=="inset") { ?> selected="selected" <?php } ?>>Inset</option>
                                                <option value="outset" <?php if($border_type[3]=="outset") { ?> selected="selected" <?php } ?>>Outset</option>
                                                <option value="ridge" <?php if($border_type[3]=="ridge") { ?> selected="selected" <?php } ?>>Ridge</option>
                                                <option value="none" <?php if($border_type[3]=="none") { ?> selected="selected" <?php } ?>>None</option>
 
                                            </select>
                                            #<input type="text" class="form-control-10" maxlength="6" name="afc_border_color_3" id="afc_border_color_3" value="<?php echo $border_color[3]; ?>" />


                                            <select name="afc_border_rounded_3" id="afc_border_rounded_3" class="form-control-10" style="width:150px;">
                                                <option value="0" <?php if($border_round[3]=="0") { ?> selected="selected" <?php } ?>>Straight Cornor</option>
                                                <option value="1" <?php if($border_round[3]=="1") { ?> selected="selected" <?php } ?>>Round Cornor</option>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="CustomCss">Custom CSS:</label>
                                            <textarea class="form-control" rows="3" placeholder="Enter Your Custom Css Here..." id="afc_css" name="afc_css"><?php echo isset($data['afc_css'])? $data['afc_css']: "";?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="ShowContent"> Show on HomePage:</label>
                                            <label><input type="radio" name="afc_cpts_home" <?php if( $data['afc_cpts_home']==1 ) { ?> checked="checked" <?php } ?> value="1">Yes</label>
                                            <label><input type="radio" name="afc_cpts_home" <?php if( $data['afc_cpts_home']==0) { ?> checked="checked" <?php } ?> value="0">No</label>
                                        </div>
                                        <div class="form-group">
                                            <label for="ShowContent"> Show on Posts:</label>
                                            <label><input type="radio" name="afc_cpts_posts" <?php if($data['afc_cpts_posts']==1 || empty($data['afc_cpts_posts'])) { ?> checked="checked" <?php } ?> value="1">All Posts</label>
                                            <label><input type="radio" name="afc_cpts_posts" <?php if($data['afc_cpts_posts']==0) { ?> checked="checked" <?php } ?> value="0">Only Selected Posts</label>                                        </div>
                                        <div class="form-group" id="all_selected_posts_display_area" <?php if ($data['afc_cpts_posts']==0) { echo 'style="display:block;"';} ?>>
                                        	<select name="afc_slected_posts" id="afc_slected_posts" class="form-control" multiple="multiple" style="height:150px;">
                                            <?php
                                            	foreach ( $postlist as $post ) {												
											?>
                                            	<option value="<?php echo $post->ID;?>" <?php if(in_array($post->ID, $selected_posts)) {  ?> selected="selected" <?php } ?>><?php echo $post->post_title;?></option>
                                            <?php	
												}
											?>
                                            </select>
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="ShowContent"> Show on Pages:</label>
                                            <label><input type="radio" name="afc_cpts_pages" <?php if($data['afc_cpts_pages']==1 || $data['afc_cpts_pages']=="") { ?> checked="checked" <?php } ?> value="1">All Pages</label>
                                            <label><input type="radio" name="afc_cpts_pages" <?php if($data['afc_cpts_pages']==0) { ?> checked="checked" <?php } ?> value="0">Only Selected Pages</label>                                        </div>
                                        <div class="form-group" id="all_selected_pages_display_area" <?php if ($data['afc_cpts_pages']==0) { echo 'style="display:block;"';} ?>>
                                        	<select name="afc_slected_pages" id="afc_slected_pages" class="form-control" multiple="multiple" style="height:150px;">
                                            <?php
                                            	foreach ( $pagelist as $page ) {
											?>
                                            	<option value="<?php echo $page->ID;?>" <?php if(in_array($page->ID, $selected_pages)) {  ?> selected="selected" <?php } ?>><?php echo $page->post_title;?></option>
                                            <?php	
												}
											?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="ShowContent"> Show on Category Pages:</label>
                                            <label><input type="radio" name="afc_cpts_category" <?php if($data['afc_cpts_category']==1 || $data['afc_cpts_category']=="") { ?> checked="checked" <?php } ?> value="1">All Category Pages</label>
                                            <label><input type="radio" name="afc_cpts_category" <?php if($data['afc_cpts_category']==0) { ?> checked="checked" <?php } ?> value="0">Only Selected Categories</label>                                        </div>
                                        <div class="form-group" id="all_selected_category_display_area" <?php if ($data['afc_cpts_category']==0) { echo 'style="display:block;"';} ?>>
                                        	<select name="afc_slected_category" id="afc_slected_category" class="form-control" multiple="multiple" style="height:150px;">
                                            <?php
                                            	foreach ( $categorieslist as $category ) {
											?>
                                            	<option value="<?php echo $category->cat_ID;?>" <?php if(in_array($category->cat_ID, $selected_category)) { ?> selected="selected" <?php } ?>><?php echo $category->cat_name;?></option>
                                            <?php	
												}
											?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="ShowContent"> Show on Custom Post Type:</label>
                                            <label><input type="radio" name="afc_cpts_cpts" <?php if($data['afc_cpts_cpts']==1 || $data['afc_cpts_cpts']=="") { ?> checked="checked" <?php } ?> value="1">All Custom Post Types</label>
                                            <label><input type="radio" name="afc_cpts_cpts" <?php if($data['afc_cpts_cpts']==0) { ?> checked="checked" <?php } ?> value="0">Only Selected Custom Post Types</label>                                        </div>
                                        <div class="form-group" id="all_selected_cpts_display_area" <?php if ($data['afc_cpts_cpts']==0) { echo 'style="display:block;"';} ?>>
                                        	<select name="afc_slected_cpts" id="afc_slected_cpts" class="form-control" multiple="multiple" style="height:150px;">
                                            <?php
                                            	foreach ( $cptslist as $cpts ) {
											?>
                                            	<option value="<?php echo $cpts;?>" <?php if(in_array($cpts, $selected_cpts)) { ?> selected="selected" <?php } ?>><?php echo $cpts;?></option>
                                            <?php	
												}
											?>
                                            </select>
                                        </div>                
                                        <div class="form-group">
                                            <label for="Status">Status:</label>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="afc_status" value="1" <?php echo $checked ?>>
                                                    Active
                                                </label>
                                                <label>
                                           <input type="radio" name="afc_status" value="0" <?php echo isset($checked2) ? $checked2 : ''; ?> >
                                                    Inactive
                                                </label>                                        
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Status">Control Impression:</label>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="afc_impression_control" value="1" <?php echo $checked3 ?>>
                                                    Yes
                                                </label>
                                                <label>
                                  				<input type="radio" name="afc_impression_control" value="0" <?php echo isset($checked4) ? $checked4 : ''; ?> >
                                                    No
                                                </label>                                        
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Status">Control Other Devices:</label>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="afc_control_devices" value="1" <?php echo $checked5 ?>>
                                                    Responsive
                                                </label>
                                                <label>
                                  				<input type="radio" name="afc_control_devices" value="2" <?php echo isset($checked6) ? $checked6 : ''; ?> >
                                                    Hide On other Devices
                                                </label>                    
                                                        
                                            </div>
                                        </div>
                                         
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                    	<input type="hidden" name="id" value="<?php echo isset($data['id_afc']) ? $data['id_afc'] : "";?>">
                                        <input type="hidden" name="action" value="<?php echo $action;?>">                                       
                                        <button type="submit" class="btn btn-primary" name="submit" id="btn_afc">Submit</button>
                                    </div>
                                </form>
                            </div>
                            
            
            <?php } ?>
            
            
            	<div class="afc_footer">
                	Plugin Developer : <a href="http://www.innovativeroots.com/" target="_blank">innovativeroots.com</a>
                </div>
            </div>
        </div>
        </div>
        
    </div>    
</div>
