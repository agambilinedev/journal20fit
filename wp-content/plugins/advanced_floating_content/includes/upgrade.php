<?php
	global $wpdb;
	$table = $wpdb->prefix . 'afc';
	$data_cols = array();	
	$req_cols = array(
		'0' => array('col'=>'id_afc','type'=>'int(11)','default'=>''),
		'1' => array('col'=>'afc_title','type'=>'varchar(255)','default'=>''),
		'2' => array('col'=>'afc_content','type'=>'text','default'=>''),
		'3' => array('col'=>'afc_position_content','type'=>'varchar(100)','default'=>''),
		'4' => array('col'=>'afc_position_y','type'=>'varchar(10)','default'=>'top'),
		'5' => array('col'=>'afc_position_x','type'=>'varchar(10)','default'=>'left'),
		'6' => array('col'=>'afc_position_center','type'=>'varchar(10)','default'=>'yes'),
		'7' => array('col'=>'afc_close','type'=>'varchar(10)','default'=>'yes'),
		'8' => array('col'=>'afc_margin_top','type'=>'int(11)','default'=>'0'),
		'9' => array('col'=>'afc_margin_bottom','type'=>'int(11)','default'=>'0'),
		'10' => array('col'=>'afc_margin_right','type'=>'int(11)','default'=>''),
		'11' => array('col'=>'afc_margin_left','type'=>'int(11)','default'=>''),
		'12' => array('col'=>'afc_width','type'=>'int(11)','default'=>'100'),
		'13' => array('col'=>'afc_width_unit','type'=>'varchar(10)','default'=>'px'),
		'14' => array('col'=>'afc_bg_color','type'=>'varchar(10)','default'=>'ffffff'),
		'15' => array('col'=>'afc_border_pixels','type'=>'varchar(255)','default'=>'1,1,1,1'),
		'16' => array('col'=>'afc_border_type','type'=>'varchar(255)','default'=>'solid,solid,solid,solid'),
		'17' => array('col'=>'afc_border_color','type'=>'varchar(255)','default'=>'cccccc,cccccc,cccccc,cccccc'),
		'18' => array('col'=>'afc_border_rounded','type'=>'varchar(255)','default'=>'0,0,0,0'),
		'19' => array('col'=>'afc_css','type'=>'text','default'=>''),
		'20' => array('col'=>'afc_cpts_home','type'=>'tinyint(1)','default'=>'1'),
		'21' => array('col'=>'afc_cpts_posts','type'=>'tinyint(1)','default'=>'1'),
		'22' => array('col'=>'afc_slected_posts','type'=>'text','default'=>''),
		'23' => array('col'=>'afc_cpts_pages','type'=>'tinyint(1)','default'=>'1'),
		'24' => array('col'=>'afc_slected_pages','type'=>'text','default'=>''),
		'25' => array('col'=>'afc_cpts_category','type'=>'tinyint(1)','default'=>'1'),
		'26' => array('col'=>'afc_slected_category','type'=>'text','default'=>''),
		'27' => array('col'=>'afc_cpts_cpts','type'=>'tinyint(1)','default'=>'1'),
		'28' => array('col'=>'afc_slected_cpts','type'=>'text','default'=>''),		
		'29' => array('col'=>'afc_status','type'=>'tinyint(1)','default'=>'1'),
		'30' => array('col'=>'afc_impression','type'=>'int(11)','default'=>'0'),
		'31' => array('col'=>'afc_impression_control','type'=>'tinyint(1)','default'=>'0'),
		'32' => array('col'=>'afc_control_devices','type'=>'tinyint(1)','default'=>'1'),
		'33' => array('col'=>'afc_screen_size','type'=>'varchar(10)','default'=>''),
		'34' => array('col'=>'afc_position_place','type'=>'varchar(15)','default'=>'fixed')
			
	);	
	$sql_columns = "SHOW COLUMNS FROM `".$table."`";
	$results_col = $wpdb->get_results($sql_columns, ARRAY_A);
	foreach($results_col as $data)
	{
		$data_cols[] = $data['Field'];
	}
	foreach($req_cols as $cols)
	{	
		if(!in_array($cols['col'],$data_cols))
		{
			if($cols['default']!=""){ $default = "DEFAULT  '".$cols['default']."'";}
			$alter_sql = "ALTER TABLE `".$table."` ADD  `".$cols['col']."` ".$cols['type']." NOT NULL ".$default."";
			$wpdb->query($alter_sql);
		}
	}
?>