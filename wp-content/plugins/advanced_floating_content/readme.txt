﻿=== Advanced Floating Content ===
Contributors: Muhammad Bilal
Email: bilal.rafique@gmail.com
Tags: contents, popup, static block, advertising sidebar, ctr increasing widget, ctr plugin, floating sidebar, floating wordpress sidebar, promotion, scrolling banner, scrolling sidebar, sticky advertising, sticky sidebar, wordpress sticky plugin
Requires at least: 3.0
Tested up to: 4.1
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Advance Floating Content is usefull and powerfull plugin can be used for multipurpose, if you want to show a content or banner or any advertisement Ads or any type of promotional content at fixed defined position then this is the best plugin for you.

== Description ==

Big Reason of buy this

if you love your visitors and want to engage it on pages or you have some promotional content to displays on website and that will count impressions of your advertisement content block so the progress of how many visitors see your special content.

by using our powerful plugin you can achieve your goal and you can enhance your website.

== Features ==

* Advanced Floating Content can be used for advertisement or promotions
* Unlimited contents can be added
* Manage different promotions on different post types
* Multiple promotional content can be added in anywhere home, pages or posts
* Every Promotional content or ads will count impressions and can be seen from plugin admin area.
* Advanced level of customization for each ads/content, Used wordpress editor to design your content.
* Options to show close/hide button on content.
* if any of content annoying you then you can close it by clicking close button.
* Hide on other devices without causing any error
* Easy and lightweight coding

==Customization==

* this plugin is can be customized with very basic php knowledge. As Advanced Floating Content is created in mind to easy customization for buyers/developers who can be customized easily according to their requirements.


== Installation ==

1. Upload the `advanced_floating_content` folder to the `/wp-content/plugins/` directory on your web server.
2. Activate the plugin through the _Plugins_ menu in WordPress.
3. Advanced Floating Content should be displayed on left menu.
4. From dashboard you can view all promotional contents, add new or edit existing or delete existing content block.

== Changelog ==

=1.1 = (7 Jan 2015)
add responsive feature to auto fit into mobile devices

= 1.0 =

* Initial release.