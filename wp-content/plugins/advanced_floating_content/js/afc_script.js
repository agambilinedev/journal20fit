jQuery(document).ready(function($){
  if ( jQuery('#datatable > tr').length > 1 ) {jQuery('#datatable').dataTable(); }
  
  
  
  jQuery('#afc_border_color_0').colpick({
		layout:'hex',
		colorScheme:'dark',
		submit:0,
		onChange:function(hsb,hex,rgb,el,bySetColor) {
			jQuery(el).css('border-color','#'+hex);
			// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
			if(!bySetColor) $(el).val(hex);
		}
	}).keyup(function(){
		jQuery(this).colpickSetColor(this.value);
	});
	jQuery('#afc_border_color_1').colpick({
		layout:'hex',
		colorScheme:'dark',
		submit:0,
		onChange:function(hsb,hex,rgb,el,bySetColor) {
			jQuery(el).css('border-color','#'+hex);
			// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
			if(!bySetColor) $(el).val(hex);
		}
	}).keyup(function(){
		jQuery(this).colpickSetColor(this.value);
	});
	jQuery('#afc_border_color_2').colpick({
		layout:'hex',
		colorScheme:'dark',
		submit:0,
		onChange:function(hsb,hex,rgb,el,bySetColor) {
			jQuery(el).css('border-color','#'+hex);
			// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
			if(!bySetColor) $(el).val(hex);
		}
	}).keyup(function(){
		jQuery(this).colpickSetColor(this.value);
	});
	jQuery('#afc_border_color_3').colpick({
		layout:'hex',
		colorScheme:'dark',
		submit:0,
		onChange:function(hsb,hex,rgb,el,bySetColor) {
			jQuery(el).css('border-color','#'+hex);
			// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
			if(!bySetColor) $(el).val(hex);
		}
	}).keyup(function(){
		jQuery(this).colpickSetColor(this.value);
	});
	jQuery('#afc_bg_color').colpick({
		layout:'hex',
		colorScheme:'dark',
		submit:0,
		onChange:function(hsb,hex,rgb,el,bySetColor) {
			jQuery(el).css('border-color','#'+hex);
			// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
			if(!bySetColor) $(el).val(hex);
		}
	}).keyup(function(){
		jQuery(this).colpickSetColor(this.value);
	});
	
	jQuery('#btn_afc').click(function() {		
		var showspages = ['afc_cpts_posts','afc_cpts_pages','afc_cpts_category','afc_cpts_cpts'];
		var seldropdown = ['afc_slected_posts', 'afc_slected_pages', 'afc_slected_category','afc_slected_cpts'];
		for (var j in showspages) {
			if(jQuery("input[name="+showspages[j]+"]:checked").val()==1)
			{
				jQuery('#'+seldropdown[j]).remove();
			}
		}
		for (var i in seldropdown) {
			
			  thisValue = jQuery('#'+seldropdown[i]+' option:selected').map(function() {
				return this.value;
			}).get();			
			jQuery('#'+seldropdown[i]).append(jQuery('<option></option>').val(thisValue).html(""));
			jQuery('#'+seldropdown[i]).removeAttr("selected");
			jQuery('select[name^="'+seldropdown[i]+'"] option[value="'+thisValue+'"]').attr("selected","selected");
		}		
	});
	
	jQuery('input[type="radio"]').click(function(){
		var value = jQuery(this).attr("value");
		var name = jQuery(this).attr("name");
		var display = name.substring(name.indexOf('_') +6);
		if(value==0)
		{
			jQuery("#all_selected_"+display+"_display_area").show();
		}
		else
		{
			jQuery("#all_selected_"+display+"_display_area").hide();
		}	  
	});
	jQuery('input[name="afc_control_devices"]').click(function(){
		var value = jQuery(this).attr("value");
		var name = jQuery(this).attr("name");
		if(value==3)
		{
			jQuery("#all_selected_size_display_area").show();
		}
		else
		{
			jQuery("#all_selected_size_display_area").hide();
		}	  
	});
});