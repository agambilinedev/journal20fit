<?php

/*
Plugin Name: Advanced Floating Content (shared on wplocker.com)
Plugin URI: http://www.innovativeroots.com/plugins/advanced-floating-content/
Version: 2.2
Description: Advanced Floating Content is usefull and powerfull plugin can be used for multipurpose, if you want to show a content or banner or any advertisement Ads or any type of promotional content at fixed defined position then this is the best plugin for you.
Author: Muhammad Bilal
Author URI: http://www.innovativeroots.com/
Company: Innovative Roots
Company URI: http://www.innovativeroots.com/
* License: GPL2
*/
define( 'WP_AFC_PATH_PLUGIN', dirname(__FILE__) . '/' );
add_action('init', 'advanced_floating_content_init_method');
function advanced_floating_content_init_method() {
    wp_enqueue_script('jquery');
	if(is_admin())
	{		
		if(isset($_REQUEST['page']))
		{			
			if($_REQUEST['page']=="afc_floating_contents" || $_REQUEST['page']=="afc_add_new")
			{
				wp_register_style('afc-css',plugins_url('css/afc_css.css', __FILE__));
				wp_enqueue_style( 'afc-css' );
				wp_register_style('font-awesome',plugins_url('css/font-awesome.min.css', __FILE__));
				wp_enqueue_style( 'font-awesome' );
				wp_register_style('color-picker',plugins_url('css/colpick.css', __FILE__));
				wp_enqueue_style( 'color-picker' );
				wp_register_style('css-admin-datatables-css',plugins_url('datatables/dataTables.bootstrap.css', __FILE__));
				wp_enqueue_style( 'css-admin-datatables-css' );
				wp_enqueue_script('colpick', plugins_url('js/colpick.js', __FILE__));			
				wp_enqueue_script('colpick');
				wp_enqueue_script('afc_script', plugins_url('js/afc_script.js', __FILE__));			
				wp_enqueue_script('afc_script');				
				wp_enqueue_script('script-datatables', plugins_url('datatables/jquery.dataTables.js', __FILE__));			
				wp_enqueue_script('script-datatables');
				wp_enqueue_script('script-datatables-bootstrap', plugins_url('datatables/dataTables.bootstrap.js', __FILE__));			
				wp_enqueue_script('script-datatables-bootstrap');
			}
		}
	}
}
add_action( 'admin_menu', 'advanced_floating_content_menu');
function advanced_floating_content_menu() { 
	add_menu_page('Advanced Floating Content', 'Advanced Floating Content', 'manage_options', 'afc_floating_contents', 'advanced_floating_content_all','',22 );
	add_submenu_page( 'afc_floating_contents', 'Advanced Floating Content','Add New', 'manage_options','afc_add_new', 'advanced_floating_content_new');
}
	add_action('wp_ajax_controlImpressions', 'controlImpressions' );
	add_action( 'wp_ajax_nopriv_controlImpressions', 'controlImpressions' );
	function controlImpressions() {
		extract($_POST);
		global $wpdb;
    	$table = $wpdb->prefix . 'afc_impressions';
		$user_data =  explode(",",$data);
		$dsql = "INSERT INTO `".$table."` (`id_afc`,`user_ip`) VALUES('".$user_data[1]."','".$user_data[0]."')";
		if($wpdb->query($dsql) === FALSE)
		{
		}
		die;
	}
/* Runs when plugin is activated */
register_activation_hook(__FILE__,'advanced_floating_content_activation');
function advanced_floating_content_activation() {
	global $wpdb;
    $table = $wpdb->prefix . 'afc';
	$table_inner = $wpdb->prefix . 'afc_impressions';
	$dsql = "DROP TABLE IF EXISTS `".$table."`";
	$sql = "CREATE TABLE IF NOT EXISTS ".$table." (
     `id_afc` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
     `afc_title` varchar( 255  )  DEFAULT NULL ,
	 `afc_content` text,	 
	 `afc_position_content` varchar( 100  )  NOT  NULL,	 
	 `afc_position_y` varchar( 10  )  NOT  NULL DEFAULT  'top',
	 `afc_position_x` varchar( 10  )  NOT  NULL DEFAULT  'left',
	 `afc_position_center` varchar( 10  )  NOT  NULL DEFAULT  'yes',
	 `afc_close` varchar( 10  )  NOT  NULL DEFAULT  'yes',
	 `afc_margin_top` int( 11  )  NOT  NULL DEFAULT  '0',
	 `afc_margin_bottom` int( 11  )  NOT  NULL DEFAULT  '0',
	 `afc_margin_right` int( 11  )  NOT  NULL ,
	 `afc_margin_left` int( 11  )  NOT  NULL ,
	 `afc_width` int( 11  )  NOT  NULL DEFAULT  '100',
	 `afc_width_unit` varchar( 10  )  NOT  NULL DEFAULT  'px',
	 `afc_bg_color` varchar( 10  )  NOT  NULL DEFAULT  'ffffff',
	 `afc_border_pixels` varchar( 255  )  NOT  NULL DEFAULT  '1,1,1,1',
	 `afc_border_type` varchar( 255  )  NOT  NULL DEFAULT  'solid,solid,solid,solid',
	 `afc_border_color` varchar( 255  )  NOT  NULL DEFAULT  'cccccc,cccccc,cccccc,cccccc',
	 `afc_border_rounded` varchar( 255  )  NOT  NULL DEFAULT  '0,0,0,0',
	 `afc_css` text,
	 `afc_cpts_home` tinyint( 1  )  NOT  NULL DEFAULT  '1',
	 `afc_cpts_posts` tinyint( 1  )  NOT  NULL DEFAULT  '1',
	 `afc_slected_posts` text DEFAULT NULL,
	 `afc_cpts_pages` tinyint( 1  )  NOT  NULL DEFAULT  '1',
	 `afc_slected_pages` text DEFAULT NULL,
	 `afc_cpts_category` tinyint( 1  )  NOT  NULL DEFAULT  '1',
	 `afc_slected_category` text DEFAULT NULL,
	 `afc_cpts_cpts` tinyint( 1  )  NOT  NULL DEFAULT  '1',
	 `afc_slected_cpts` text DEFAULT NULL,	 
	 `afc_status` tinyint( 1  )  NOT  NULL DEFAULT  '1',
	 `afc_impression` int( 11  )  NOT  NULL DEFAULT  '0',
	 `afc_impression_control` tinyint( 1 )  NOT  NULL DEFAULT  '0',
	 `afc_control_devices` tinyint( 1 )  NOT  NULL DEFAULT  '1',
	 `afc_screen_size`  varchar(10) NOT NULL,
	 `afc_position_place` varchar(15) NOT NULL DEFAULT 'fixed',
	 `add_date` timestamp NOT  NULL  DEFAULT CURRENT_TIMESTAMP
    )  ENGINE=MyISAM;";
	$sql_imp = "CREATE TABLE IF NOT EXISTS ".$table_inner." (	
	  `id` bigint(20) NOT NULL AUTO_INCREMENT,
	  `id_afc` int(11) NOT NULL,
	  `user_ip` varchar(256) NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=MyISAM;";
   	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
	dbDelta( $sql_imp );
	include_once( WP_AFC_PATH_PLUGIN . 'includes/upgrade.php' );
}
function advanced_floating_content_all(){
	include_once( WP_AFC_PATH_PLUGIN . 'includes/dashboard.php' );
}
function advanced_floating_content_new(){
	include_once( WP_AFC_PATH_PLUGIN . 'includes/new.php' );
}
function advanced_floating_content()
{
	include_once( WP_AFC_PATH_PLUGIN . 'includes/view.php' );
}
class advanced_floating_content extends WP_Widget {
	// constructor
	function advanced_floating_content() {
		parent::WP_Widget(false, $name = __('Advanced Floating Content', 'wp_widget_plugin') );
	}
}
// register widget

add_action('widgets_init', create_function('', 'return register_widget("advanced_floating_content");'));
add_action( 'wp_footer', 'advanced_floating_content', 5 );
?>