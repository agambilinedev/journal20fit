jQuery(document).ready(function($){
  
  
  $('a', 'div.wrap ul.wpb-esh-bg-list').click(function(){
    var obj = $(this);
    var bgimg = $('div.wpb-esh-img', obj).attr('style');
    
    $('div.wpb-active', 'ul.wpb-esh-bg-list').removeClass('wpb-active');
    $('div.wpb-esh-img', obj).addClass('wpb-active');
    $('#wpb-esh-settings-bg').val($('div.wpb-esh-img', obj).attr('data-bg'));
    
    return false;
  });
  
  
});