<?php
 /*
  * Plugin name: Easy Social Hover (shared on wplocker.com)
  * Version: 1.0.5
  * Author: WpBrickr
  * Plugin URI: http://easy-social-hover.wpbrickr.com
  * Author URI: http://www.wpbrickr.com
  * Description: Quickly & Easily add Social Network Sharing features to your images!
  */
  
  define('WPB_ESH_KEY', 'wpb_loh');
  
  require_once 'wpb.gui.php';
  
  class wpb_esh {
    
   private static $wpb_esh_effects = array('optgroup_start_basic' => 'Basic',
                                           'always' => 'Always visible',
                                           'fade-in' => 'Fade In',
                                           'optgroup_end_basic' => 'Basic');
    
    function init() {
      if (is_admin()) {
        self::default_settings();
        
        // Add additional fields to WP Media
        add_filter("attachment_fields_to_edit", array(__CLASS__, 'wpb_image_attachment_fields_to_edit'), null, 2);
        add_filter("attachment_fields_to_save", array(__CLASS__, 'wpb_image_attachment_fields_to_save'), null, 2);
        
        // Add additional class tags to WP Images on frontend
        add_filter('get_image_tag_class', array(__CLASS__, 'add_class_to_image'), null, 99);
        // Add Additional links
        add_action('plugin_action_links_' . basename(dirname(__FILE__)) . '/' . basename(__FILE__), array(__CLASS__, 'plugin_action_links'));
        // add menu page
        add_action('admin_menu', array(__CLASS__, 'register_menu'));
        // enqueue scripts in admin
        add_action('admin_enqueue_scripts', array(__CLASS__, 'admin_enqueue_scripts'));
      } else {
        add_action('wp_enqueue_scripts', array(__CLASS__, 'enqueue_scripts_and_styles'));
        add_action('wp_print_footer_scripts', array(__CLASS__, 'print_custom_scripts'));
      }
      
      add_action('wp_ajax_wpb_url_to_id', array(__CLASS__, 'ajax_url_to_id'));
      add_action('wp_ajax_nopriv_wpb_url_to_id', array(__CLASS__, 'ajax_url_to_id'));
    } // init
    
    
    function admin_enqueue_scripts() {
      $screen = get_current_screen();

      if ($screen->id == 'toplevel_page_wpb-esh') {
        wp_enqueue_script('wpb-esh-backend-jquery', plugins_url('js/wpb.gui.js', __FILE__), array('jquery'), '1.0', true);
        wp_enqueue_style('wpb-esh-backend-style', plugins_url('css/style.css', __FILE__), array(), '1.0');
      }
    } // admin_enqueue_scripts
    
    
    function ajax_url_to_id() {
      global $wpdb;
      $url = trim($_POST['url']);
      $post_id = url_to_postid($url);
      
      $post = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "posts WHERE guid='" . $url . "'");
      echo get_attachment_link($post->ID);
      die();
    } // wpb_url_to_id
    
    
    function add_class_to_image($class, $id, $align, $size){
      global $post;
      // Fetch image effects from meta
      $enabled  = get_post_meta($id, "_wpb_esh_enabled", true);

      // Update image class
      if ($enabled && $enabled != '2') {
        $class  .= ' wp-image-esh';
      }
    
      return $class;
    } // add_class_to_image
   
   
   /* Add links to plugins page
    * @since 1.0
    */
    function plugin_action_links($links) {
      $settings_link = '<a href="admin.php?page=wpb-esh" title="Easy Social Hover Settings">Settings</a>';
      $doc_link = '<a href="' . plugins_url('/documentation/index.html', __FILE__) . '" target="_blank" title="Easy Social Hover Documentation">Documentation</a>';
      array_unshift($links, $doc_link);
      array_unshift($links, $settings_link);

      return $links;
    } // plugin_action_links
    
   /* Register menu
    * @since 1.0
    */
    function register_menu() {
      add_menu_page('Easy Social Hover', 'Easy Social Hover', 'manage_options', 'wpb-esh', array('wpb_esh_gui', 'settings'));
    } // register_menu
    
    
    function wpb_image_attachment_fields_to_edit($form_fields, $post) {
      $options = array();
      $options[0] = 'Last Known State';
      $options[1] = 'Enabled';
      $options[2] = 'Disabled';
      $setup = get_post_meta($post->ID, "_wpb_esh_enabled", true);
      
      $form_fields["wpb_esh_effect"] = array("label" => "Enable Social Hover",
                                             "input" => "text",
                                             "value" => get_post_meta($post->ID, "_wpb_esh_enabled", true));

      $form_fields["wpb_esh_effect"]["label"] = "";  
      $form_fields["wpb_esh_effect"]["input"] = "html";  
      $form_fields["wpb_esh_effect"]["html"] = "<span style='font-weight:bold;text-shadow:0 1px 0 #FFFFFF;color:#000;'>Enable Social Hover</span>
      <select name='attachments[{$post->ID}][wpb_esh_enabled]' id='attachments[{$post->ID}][wpb_esh_enabled]'>
       " . self::create_dropdown($options, $setup) . "
      </select>";
    
  
       return $form_fields;  
    } // wpb_image_attachment_fields_to_edit
  
  
    function wpb_image_attachment_fields_to_save($post, $attachment) {
      // Fetch image effects from meta
      $hover_effect  = get_post_meta($post['ID'], "_wpb_esh_enabled", true);
    
      
      if( isset($attachment['wpb_esh_enabled']) ){
        // Save effect of the image
        update_post_meta($post['ID'], '_wpb_esh_enabled', $attachment['wpb_esh_enabled']);
      }

      return $post;   
    } // wpb_image_attachment_fields_to_save
   
   
    function enqueue_scripts_and_styles() {
      $options = get_option(WPB_ESH_KEY);

      if (isset($options['force-jquery']) && $options['force-jquery'] == '1') {
        wp_enqueue_script('jquery');
      } // force jQuery
    
      wp_enqueue_script("wpb-esh-js", plugins_url('/js/jquery.esh.js', __FILE__), array('jquery'), '1.0.0', true);
      wp_enqueue_style("wpb-esh-css", plugins_url('/css/esh.css', __FILE__), array(), '1.0.0', 'all');

      wp_enqueue_script("wpb-esh-jquery-ui", plugins_url('/js/jquery-ui-1.9.2.custom.min.js', __FILE__), array('jquery'), '1.9.2', true);
      wp_enqueue_style("wpb-esh-jquery-ui-css", plugins_url('/css/jquery-ui-1.9.2.custom.min.css', __FILE__), array(), '1.0.0', 'all');
    } // enqueue_scripts_and_styles
   
   
    function print_custom_scripts() {
      global $post;
      $output = '';
     
      $options = get_option(WPB_ESH_KEY);
      $socials = get_option(WPB_ESH_KEY . '_socials');
      
      if ($options['bg_opacity'] == '0') {
        $bg_opacity = '0';
      } else {
        $bg_opacity = ($options['bg_opacity']/100);
      }
      
      $background = $options['bg'];
      $facebook   = plugins_url('images/facebook.png', __FILE__);
      $google     = plugins_url('images/google.png', __FILE__);
      $in         = plugins_url('images/in.png', __FILE__);
      $twitter    = plugins_url('images/twitter.png', __FILE__);
      $pinterest  = plugins_url('images/pinterest.png', __FILE__);
     
      $output .= '<script type="text/javascript">
                  jQuery(document).ready(function($){
                  $.ajaxSetup({async:false});
                  // Preload the bg
                  $("<img src=\"' . $background . '\" />");
                  $("<img src=\"' . $facebook . '\" />");
                  $("<img src=\"' . $google . '\" />");
                  $("<img src=\"' . $in . '\" />");
                  $("<img src=\"' . $twitter . '\" />");
                  $("<img src=\"' . $pinterest . '\" />");
                 
                  $("img[class*=\'wp-image-esh\']").each(function() {
                    var url = "";';
                    
      $output .= 'var img_src = $(this).attr(\'src\');';
                   
                   
      if ($options['share'] == 'page') {
        $share_url = get_permalink($post->ID);
        $output .= 'url = "' . $share_url . '";';
      } else if ($options['share'] == 'img_src') {
        $output .= 'url = $(this).attr(\'src\');';
      } else if ($options['share'] == 'attachment_url') {
        $output .= 'var img_src = $(this).attr(\'src\');';
        $output .= 'if (typeof ajaxurl === "undefined") {
                      var ajaxurl = "' . admin_url('admin-ajax.php') . '";
                    }';
        $output .= '$.post(ajaxurl, {"action":"wpb_url_to_id", "url":img_src}, function(r) {
                      url = r;
                    });';
      }
                     
      $output .= '$(this).esh({"background":"' . $background . '",
                               "effect":"' . $options['effect'] . '", 
                               "direction":"' . $options['direction'] . '",
                               "opacity":' . $bg_opacity . ',';
      
      if (isset($socials['facebook'])) {
        $output .= '"fb_url":"https://www.facebook.com/sharer/sharer.php?u=" + url,';
      }
      
      if (isset($socials['twitter'])) {      
        $output .= '"twitter":"https://twitter.com/share?url=" + url + "&text=&counturl=" + url,';
      }
      
      if (isset($socials['googleplus'])) {
        $output .= '"google":"https://plus.google.com/share?url=" + url,';
      }
      
      if (isset($socials['linkedin'])) {
        $output .= '"linkedin":"http://www.linkedin.com/shareArticle?mini=true&url=" + url + "&summary=&source=" + url,';
      }
      
      if (isset($socials['pinterest'])) {
        $output .= '"pinterest":"http://pinterest.com/pin/create/button/?url=" + url + "&media=" + img_src';
      }
                               
        $output .= '});
                 });
                 });
                 </script>';
     
     echo $output;
    } // print_custom_scripts
   
   
    function create_dropdown($options, $selected = '', $o = true) {
      $output = '';
      if (is_array($options)) {
      
        foreach ($options as $k => $v) {
          if (preg_match('/optgroup_start_(.*)/', $k)) {
            $output .= '<optgroup label="' . $v . '">';
          } elseif (preg_match('/optgroup_end_(.*)/', $k)) {
            $output .= '</optgroup>';
          } elseif ($k == $selected) {
            $output .= '<option value="' . $k . '" selected="selected">' . $v . '</option>';
          } else {
            $output .= '<option value="' . $k . '">' . $v . '</option>';
          }
        }
      }

      if ($o) {
        return $output;
      } else {
        echo $output;
      }
    } // create_dropdown
    
    
    function default_settings() {
      $options = get_option(WPB_ESH_KEY);
      $socials = get_option(WPB_ESH_KEY . '_socials');
      
      if (!$options || !isset($options['effect'])) {
        $options['effect']       = 'bounce';
        $options['force-jquery'] = '1';
        $options['direction']    = 'top-middle';
        $options['share']        = 'img_src';
        $options['bg']           = plugins_url('images/backgrounds/1.png', __FILE__);
        $options['bg_opacity']   = '50';
        update_option(WPB_ESH_KEY, $options);
      }
      
      if (!$socials) {
        $socials['facebook']  = '1';
        $socials['pinterest'] = '1';
        $socials['twitter']   = '1';
        update_option(WPB_ESH_KEY . '_socials', $socials);
      }
      
    } // default_settings

  } // wpb_esh
  
  add_action('init', array('wpb_esh', 'init'));