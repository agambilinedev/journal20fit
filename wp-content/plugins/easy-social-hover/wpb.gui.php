<?php
 /*
  * Main Admin GUI File
  * © 2012 WpBrickr <info@wpbrickr.com>
  * All rights reserved by WpBrickr.Com
  */
  
 class wpb_esh_gui {

   
   private static $wpb_esh_socials = array('facebook', 'twitter', 'googleplus', 'pinterest', 'linkedin');
   
   private static $wpb_effect_direction = array('top-middle' => 'Top To Middle (default)',
                                                'bottom-middle' => 'Bottom To Middle',
                                                'top-bottom'   => 'Top To Bottom',
                                                'bottom-top'   => 'Bottom To Top');
                                                
   private static $wpb_effect = array('bounce'  => 'Bounce In/Out (default)',
                                      'show' => 'Show/Hide',
                                      'fade-in' => 'Fade In/Out');
                                      
   private static $wpb_share  = array('page'  => 'Page on which the image is located (default)',
                                      'img_src' => 'Image Source (direct link to image)',
                                      'attachment_url' => 'Attachment URL (WP attachment page)');
                                      
   private static $wpb_opacity = array();
                                      
   private static $wpb_bgs = array('noisy_grid' => 'Noisy Grid' );
   
   function settings() {
     for($i=0;$i<=100;$i+=5){
       self::$wpb_opacity[$i] = $i;
     }
     
     if (!current_user_can('manage_options'))  {
       wp_die('You do not have sufficient permissions to access this page.');
     }
     
    if (isset($_POST['wpb-esh-socials'])) {
      update_option(WPB_ESH_KEY . '_socials', $_POST['wpb-esh-socials']);
    }
    
    
    if (isset($_POST['wpb-esh-settings'])) {
      update_option(WPB_ESH_KEY, $_POST['wpb-esh-settings']);
    }
     
     $options = get_option(WPB_ESH_KEY);
    
     echo '<div class="wrap">
           <div class="icon32" id="icon-options-general"><br></div>
           <h2>WP Easy Social Hover Global Settings</h2>';

     echo '<form action="admin.php?page=wpb-esh" method="post">';
     settings_fields(WPB_ESH_KEY);
     
     echo '<hr/>';
    
     echo '<h3>jQuery Settings</h3>';
     echo '<table class="form-table"><tbody>';
    
     echo '<tr valign="top">
             <th scope="row">
               <label for="wpb-esh-force-jquery">Force jQuery on frontend:</label>
             </th>
           <td>
             <input type="checkbox" name="wpb-esh-settings[force-jquery]" id="wpb-esh-force-jquery" value="1" ' . self::is_checked('force-jquery') . ' /><br/>
             <span class="description">This is sometimes required when<br/> jQuery isn\'t available on frontend.</span>
           </td>
           </tr>';
           
     echo '</tbody></table>';
           
    
     echo '<h3>Hover Settings</h3>';
     echo '<table class="form-table"><tbody>';
     
     echo '<tr valign="top">
             <th scope="row">
               <label for="wpb-esh-effect">Effect on hover:</label>
             </th>
           <td>
             <select name="wpb-esh-settings[effect]" id="wpb-esh-settings-effect">
               ' . self::create_dropdown(self::$wpb_effect, $options['effect']) . '
             </select>
             <span class="description">Select what effect do you want to be used globaly upon hovering over images.</span>
           </td>
           </tr>';
     
     echo '<tr valign="top">
             <th scope="row">
               <label for="wpb-esh-direction">Effect Direction on hover:</label>
             </th>
           <td>
             <select name="wpb-esh-settings[direction]" id="wpb-esh-settings-direction">
               ' . self::create_dropdown(self::$wpb_effect_direction, $options['direction']) . '
             </select>
             <span class="description">Select what direction should effect be upon hovering over images.</span>
           </td>
           </tr>';
     
     echo '</tbody></table>';
           
     echo '<h3>Social Settings</h3>';
     echo '<table class="form-table"><tbody>';
     
     echo '<tr valign="top">
             <th scope="row">
               <label for="wpb-esh-direction">What to share:</label>
             </th>
           <td>
             <select name="wpb-esh-settings[share]" id="wpb-esh-settings-share">
               ' . self::create_dropdown(self::$wpb_share, $options['share']) . '
             </select>
             <span class="description">Select what do you want to share on social networks.</span>
           </td>
           </tr>';
    
    
     echo '<tr valign="top"><td colspan="2">';
     echo '<h3>Background pattern</h3>';
     echo '<td/><tr/>';
     
     echo '<tr valign="top"><td colspan="2">';
     self::update_backgrounds();
     echo '<td/><tr/>';
     
     echo '<tr valign="top"><td colspan="2">';
     echo '<h3>Background settings</h3>';
     echo '<td/><tr/>';
     
     echo '<tr valign="top">
             <th scope="row">
               <label for="wpb-esh-bg-opacity">Background Opacity:</label>
             </th>
     <td>';
     echo '<select name="wpb-esh-settings[bg_opacity]" id="wpb-esh-settings-bg-opacity">';
     echo self::create_dropdown(self::$wpb_opacity, $options['bg_opacity']);
     echo '</select>';
     echo '<td/><tr/>';
     
     echo '<tr valign="top"><td colspan="2">';
     echo '<h3>Active social icons</h3>';
     echo '<td/><tr/>';
     
     echo self::create_social_gui();
     
     echo '</tbody></table>';

     // Save
     echo '<p class="submit"><input type="submit" value="Save Changes" class="button-primary" name="Submit" /></p>';

     echo '</form>';

     echo '</div>';
   } // settings
   
   
   function update_backgrounds() {
     $options = get_option(WPB_ESH_KEY);
     $files   = glob(plugin_dir_path(__FILE__) . 'images/backgrounds/*');
     $remove  = plugin_dir_path(__FILE__);
     
     echo '<ul class="wpb-esh-bg-list">';
     
     foreach($files as $file) {
       $image = $file;
       $image = str_replace($remove, '', $image);
       $image = plugins_url($image, __FILE__);
       
       if ($image == $options['bg']) {
       echo '<li>
               <a href="#" class="">
                 <div class="wpb-esh-img wpb-active" style="background:url(\'' . $image . '\') top left repeat;" data-bg="' . $image . '"></div>
               </a>
             </li>';
       } else {
       echo '<li>
               <a href="#">
                 <div class="wpb-esh-img active" style="background:url(\'' . $image . '\') top left repeat;" data-bg="' . $image . '"></div>
               </a>
             </li>';         
       }
     }
     echo '<br style="clear:both;"/>';
     echo '</ul>';
     echo '<input type="hidden" name="wpb-esh-settings[bg]" id="wpb-esh-settings-bg" value="' . $options['bg'] . '" />';
   } // update_backgrounds
   
   
   function create_social_gui() {
     $output = '';
     $socials = get_option(WPB_ESH_KEY . '_socials');

     foreach (self::$wpb_esh_socials as $k => $social) {
       
       $checked = '';
       if (isset($socials[$social])) {
         $checked = 'checked="checked"';
       }
       
       $output .= '<tr valign="top">
                     <th scope="row">
                       <label for="wpb-esh-socials-' . $social . '"><img style="float:left;" src="' . plugins_url('images/gui/' . $social . '.png', __FILE__) . '" alt="' . $social . ' Icon"/>
                       <div style="height:18px;padding-top:2px;padding-left:8px;width:150px;float:left;">' . ucfirst($social) . ':</div></label>
                     </th>
                     <td>
                       <input type="checkbox" name="wpb-esh-socials[' . $social . ']" id="wpb-esh-socials-' . $social . '" value="1" ' . $checked . ' />
                     </td>
                   </tr>';
     } // foreach
     
     return $output;
   } // create_social_gui
   
   
   function is_checked($setting = '') {
     $options = get_option(WPB_ESH_KEY);
     if (isset($options[$setting])) {
       if ($options[$setting] == '1') {
         return 'checked="checked"';
       }
     }
     return '';
   } // is_checked
   
   
   function create_dropdown($options, $selected = '', $o = true) {
     $output = '';
     if (is_array($options)) {
      
       foreach ($options as $k => $v) {
         if ($k == $selected) {
           $output .= '<option value="' . $k . '" selected="selected">' . $v . '</option>';
         } else {
           $output .= '<option value="' . $k . '">' . $v . '</option>';
         }
       }
     }
    
     if ($o) {
       return $output;
     } else {
       echo $output;
     }
   } // create_dropdown
   
   
 } // class wpb_esh_gui