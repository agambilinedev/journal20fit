<div class="col-md-6 blg-typ3">
<article id="post-<?php the_ID(); ?>" <?php post_class('blog-post blgtyp3'); ?>>
<?php GLOBAL $webnus_options;
	$featured_enable = $webnus_options->webnus_blog_featuredimage_enable();
	$post_format = get_post_format(get_the_ID());
	if(!$post_format) $post_format = 'standard';
	$content = get_the_content();
	global $featured_video;
	$meta_video = $featured_video->the_meta();

// Post Thumbnail
if( !empty($featured_enable) && $post_format != 'aside' && $post_format != 'quote' && $post_format != 'link' && (has_post_thumbnail() || !empty($meta_video))) { ?>
	 <div class="blg-typ3-thumb">
		<?php if($post_format  == 'video' || $post_format == 'audio') {
					$pattern = '\\[' . '(\\[?)' . "(video|audio)" . '(?![\\w-])' . '(' . '[^\\]\\/]*' . '(?:' . '\\/(?!\\])' . '[^\\]\\/]*' . ')*?' . ')' . '(?:' . '(\\/)' . '\\]' . '|' . '\\]' . '(?:' . '(' . '[^\\[]*+' . '(?:' . '\\[(?!\\/\\2\\])' . '[^\\[]*+' . ')*+' . ')' . '\\[\\/\\2\\]' . ')?' . ')' . '(\\]?)';
					preg_match('/'.$pattern.'/s', $post->post_content, $matches);
					if( (is_array($matches)) && (isset($matches[3])) && ( ($matches[2] == 'video') || ('audio'  == $post_format)) && (isset($matches[2]))) {
					$video = $matches[0];
					echo do_shortcode($video);
					$content = preg_replace('/'.$pattern.'/s', '', $content);
					} elseif( (!empty( $meta_video )) && (!empty($meta_video['the_post_video'])) ) {
					echo do_shortcode($meta_video['the_post_video']);
					}
			} elseif( 'gallery'  == $post_format) {
					$pattern = '\\[' . '(\\[?)' . "(gallery)" . '(?![\\w-])' . '(' . '[^\\]\\/]*' . '(?:' . '\\/(?!\\])' . '[^\\]\\/]*' . ')*?' . ')' . '(?:' . '(\\/)' . '\\]' . '|' . '\\]' . '(?:' . '(' . '[^\\[]*+' . '(?:' . '\\[(?!\\/\\2\\])' . '[^\\[]*+' . ')*+' . ')' . '\\[\\/\\2\\]' . ')?' . ')' . '(\\]?)';
					preg_match('/'.$pattern.'/s', $post->post_content, $matches);
					if( (is_array($matches)) && (isset($matches[3])) && ($matches[2] == 'gallery') && (isset($matches[2]))) {
					$ids = (shortcode_parse_atts($matches[3]));				
					if(is_array($ids) && isset($ids['ids'])) { $ids = $ids['ids']; }
					echo do_shortcode('[vc_gallery img_size= "420x280" type="flexslider_slide" interval="3" images="'.$ids.'" onclick="link_no" custom_links_target="_self"]');
					$content = preg_replace('/'.$pattern.'/s', '', $content);}
			} else {
					get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'blog3_thumb' ) ); }
		?>
		<br>

	</div>
	<div class="blg-typ3-content">
<?php } else { ?>

	<div class="blg-typ3-inner">

<?php } ?>

<?php if($webnus_options->webnus_blog_social_share()) { ?>	
	<div class="post-sharing"><div class="blog-social" style="text-align:center;">
		<a class="facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" target="blank">
		<i class="fa-facebook" ></i></a>
		<a class="google" href="https://plusone.google.com/_/+1/confirm?hl=en-US&amp;url=<?php the_permalink(); ?>" target="_blank"><i class="fa-google" "></i></a>
		<a class="twitter" href="https://twitter.com/intent/tweet?original_referer=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>&amp;tw_p=tweetbutton&amp;url=<?php the_permalink(); ?><?php echo isset( $twitter_user ) ? '&amp;via='.$twitter_user : ''; ?>" target="_blank"><i class="fa-twitter" ></i></a>
		<a class="linkedin" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>&amp;title=<?php the_title(); ?>&amp;source=<?php bloginfo( 'name' ); ?>"><i class="fa-linkedin" ></i></a>
		<a class="email" href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php the_permalink(); ?>"><i class="fa-envelope" ></i></a>

<style type="text/css">
	.blog-post .blog-social {
		    margin-left: auto;
    margin-right: auto;
    padding-bottom: 20px;
float: none;
	}

</style>
	</div></div>
<?php } ?>

	
<h6 class="blog-cat"><?php the_category(', ') ?> | </h6><h6 class="blog-date"><?php the_time('F d, Y') ?></h6>
	<?php
	if(function_exists('wp_review_show_total')){wp_review_show_total(true, 'review-total-only small-thumb');}
	    if(  $webnus_options->webnus_blog_posttitle_enable() ) { 
			if( ('aside' != $post_format ) && ('quote' != $post_format)  ) { 	
				if( 'link' == $post_format ) {
					preg_match('/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i', $content,$matches);
					$content = preg_replace('/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i','', $content,1);
					$link ='';
					if(isset($matches) && is_array($matches)) $link = $matches[0]; ?>
					<h3><a href="<?php echo esc_url($link); ?>"><?php the_title() ?></a></h3> <?php
		
				} else { ?>
					<h3><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3> <?php
				}
			}
		}
		
?>		


	
<?php
// Post Content

		if($post_format == 'quote' ) echo '<blockquote>';
			echo '<p>'.webnus_excerpt(31).'</p>';
		if($post_format == 'quote') echo '</blockquote>';
		if($post_format == ('quote') || $post_format == 'aside' )
			echo '<a class="readmore" href="'. get_permalink( get_the_ID() ) . '">' . esc_html__('View Post', 'webnus_framework') . '</a>';
	?>

	</div>
<hr class="vertical-space1">
</article></div>