<?php GLOBAL $webnus_options; ?>
<section class="footer-social-bar">
	<div class="container"><div class="row">
	<ul class="footer-social-items">
	<?php
		if($webnus_options->webnus_facebook_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_facebook_ID()) .'" class="facebook"><i class="fa-facebook"></i><div><strong>'.esc_html__('Facebook','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Facebook','webnus_framework').'</span></div></a></li>';
		if($webnus_options->webnus_twitter_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_twitter_ID()) .'" class="twitter"><i class="fa-twitter"></i><div><strong>'.esc_html__('Twitter','webnus_framework').'</strong><span>'.esc_html__('Follow us on ','webnus_framework').esc_html__('Twitter','webnus_framework').'</span></div></a></li>';
		if($webnus_options->webnus_dribbble_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_dribbble_ID()).'" class="dribble"><i class="fa-dribbble"></i><div><strong>'.esc_html__('Dribbble','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Dribbble','webnus_framework').'</span></div></a></li>';
		if($webnus_options->webnus_pinterest_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_pinterest_ID()) .'" class="pinterest"><i class="fa-pinterest"></i><div><strong>'.esc_html__('Pinterest','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Pinterest','webnus_framework').'</span></div></a></li>';
		if($webnus_options->webnus_vimeo_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_vimeo_ID()) .'" class="vimeo"><i class="fa-vimeo-square"></i><div><strong>'.esc_html__('Vimeo','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Vimeo','webnus_framework').'</span></div></a></li>';
		if($webnus_options->webnus_youtube_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_youtube_ID()) .'" class="youtube"><i class="fa-youtube"></i><div><strong>'.esc_html__('Youtube','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Youtube','webnus_framework').'</span></div></a></li>';	
		if($webnus_options->webnus_google_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_google_ID()) .'" class="google"><i class="fa-google"></i><div><strong>'.esc_html__('Google Plus','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Google Plus','webnus_framework').'</span></div></a></li>';	
		if($webnus_options->webnus_linkedin_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_linkedin_ID()) .'" class="linkedin"><i class="fa-linkedin"></i><div><strong>'.esc_html__('Linkedin','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Linkedin','webnus_framework').'</span></div></a></li>';	
		if($webnus_options->webnus_rss_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_rss_ID()) .'" class="rss"><i class="fa-rss"></i><div><strong>'.esc_html__('RSS','webnus_framework').'</strong><span>'.esc_html__('Keep updated with ','webnus_framework').esc_html__('RSS','webnus_framework').'</span></div></a></li>';
		if($webnus_options->webnus_instagram_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_instagram_ID()) .'" class="instagram"><i class="fa-instagram"></i><div><strong>'.esc_html__('Instagram','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Instagram','webnus_framework').'</span></div></a></li>';	
		if($webnus_options->webnus_flickr_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_flickr_ID()) .'" class="other-social"><i class="fa-flickr"></i><div><strong>'.esc_html__('Flickr','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Flickr','webnus_framework').'</span></div></a></li>';	
		if($webnus_options->webnus_reddit_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_reddit_ID()) .'" class="other-social"><i class="fa-reddit"></i><div><strong>'.esc_html__('Reddit','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Reddit','webnus_framework').'</span></div></a></li>';
		if($webnus_options->webnus_delicious_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_delicious_ID()) .'" class="other-social"><i class="fa-delicious"></i><div><strong>'.esc_html__('Delicious','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Delicious','webnus_framework').'</span></div></a></li>';	
		if($webnus_options->webnus_lastfm_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_lastfm_ID()) .'" class="other-social"><i class="fa-lastfm"></i><div><strong>'.esc_html__('Lastfm','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Lastfm','webnus_framework').'</span></div></a></li>';
		if($webnus_options->webnus_tumblr_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_tumblr_ID()) .'" class="other-social"><i class="fa-tumblr"></i><div><strong>'.esc_html__('Tumblr','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Tumblr','webnus_framework').'</span></div></a></li>';
		if($webnus_options->webnus_skype_ID())
			echo '<li><a href="'. esc_url($webnus_options->webnus_skype_ID()) .'" class="other-social"><i class="fa-skype"></i><div><strong>'.esc_html__('Skype','webnus_framework').'</strong><span>'.esc_html__('Join us on ','webnus_framework').esc_html__('Skype','webnus_framework').'</span></div></a></li>';
	?>
	</ul>
	</div></div>
	</section>