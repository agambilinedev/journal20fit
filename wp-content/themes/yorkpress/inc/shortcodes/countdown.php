<?php

function webnus_countdown( $attributes, $content = null ) {
 	
	extract(shortcode_atts(array(
	"type"      => 'mono',
	'datetime' => '',
	'done' => '',
	
	
		), $attributes));


	$data_until = esc_attr( strtotime( $datetime ) );
	$data_done = esc_attr( $done );
	if($type=="di"){
		$label = array(
			'day' => esc_html__('DAYS', 'webnus_framework'), 
			'hours' => esc_html__('HRS', 'webnus_framework'), 
			'minutes' => esc_html__('MIN', 'webnus_framework'), 
			'seconds' => esc_html__('SEC', 'webnus_framework')
		);
	} else{
		$label = array(
			'day' => esc_html__('Days', 'webnus_framework'), 
			'hours' => esc_html__('Hours', 'webnus_framework'), 
			'minutes' => esc_html__('Minutes', 'webnus_framework'), 
			'seconds' => esc_html__('Seconds', 'webnus_framework')
		);
	}
	
 	$out  = '<div class="countdown-w ctd-' . $type . '" data-until="'. $data_until .'" data-done="'. $data_done .'" data-respond>';
	$out .= '<div class="days-w block-w"><i class="icon-w li_calendar"></i><div class="count-w"></div><div class="label-w">'. $label['day'] .'</div></div>';
	$out .= '<div class="hours-w block-w"><i class="icon-w fa-clock-o"></i><div class="count-w"></div><div class="label-w">'. $label['hours'] .'</div></div>';
	$out .= '<div class="minutes-w block-w"><i class="icon-w li_clock"></i><div class="count-w"></div><div class="label-w">'. $label['minutes'] .'</div></div>';
	$out .= '<div class="seconds-w block-w"><i class="icon-w li_heart"></i><div class="count-w"></div><div class="label-w">'. $label['seconds'] .'</div></div>';
	$out .= '</div>';
	
	return $out;

}

add_shortcode('countdown', 'webnus_countdown');		
?>