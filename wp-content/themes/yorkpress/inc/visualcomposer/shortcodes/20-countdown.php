<?php

vc_map( array(
        'name' =>'Webnus Countdown',
        'base' => 'countdown',
        "icon" => "icon-wpb-countdown",
		"description" => "Countdown",
        'category' => esc_html__( 'Webnus Shortcodes', 'webnus_framework' ),
        'params' => array(
						array(
							'type' => 'dropdown',
							'heading' => esc_html__( 'Type', 'webnus_framework' ),
							'param_name' => 'type',
							'value' => array(
								"Mono"=>"mono",
								"Di"=>"di",
								"Tri"=>"tri",
							),
							'description' => esc_html__( 'Select Countdown Type', 'webnus_framework')
						),
						array(
							'type' => 'textfield',
							'heading' => esc_html__( 'Date and Time', 'webnus_framework' ),
							'param_name' => 'datetime',
							'value' => '',
							'description' => esc_html__( 'Enter date and time (11October 2015 9:00)', 'webnus_framework')
						),
						array(
							'type' => 'textfield',
							'heading' => esc_html__( 'Finished text', 'webnus_framework' ),
							'param_name' => 'done',
							'value' => '',
							'description' => esc_html__( 'Finished text', 'webnus_framework')
						),
						
        ),
        
    ) );

?>