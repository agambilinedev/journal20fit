<?php


function webnus_customize_register( $wp_customize ) {

	class Webnus_Customize_Description_Control extends WP_Customize_Control {
		public $type = 'description';

		public function render_content() { ?>
			<span class="description customize-control-description"><?php echo $this->label; ?></span>
			<?php
		}
	}

// Logo Settings
	$wp_customize->add_section( 'logo_settings', array(
		'title'		=> esc_html__( 'Logo', 'webnus_framework' ),
		'priority'	=> 21,
		'description'=> esc_html__('To access more options please go to Appearance > Theme Options > Header Options', 'webnus_framework' ),
	) );

	// Logo
	$wp_customize->add_setting( 'logo_image', array(
		'sanitize_callback' => 'esc_url_raw',
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'upload_logo', array(
		'label'		=> esc_html__( 'Upload Logo Image', 'webnus_framework' ),
		'settings'	=> 'logo_image',
		'section'	=> 'logo_settings',
	) ) );

	// Transparent Logo
	$wp_customize->add_setting( 'transparent_logo_image', array(
		'sanitize_callback' => 'esc_url_raw',
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'upload_transparent_logo', array(
		'label'		=> esc_html__( 'Upload Transparent Logo Image', 'webnus_framework' ),
		'settings'	=> 'transparent_logo_image',
		'section'	=> 'logo_settings',
	) ) );

	// Sticky Logo
	$wp_customize->add_setting( 'sticky_logo_image', array(
		'sanitize_callback' => 'esc_url_raw',
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'upload_sticky_logo', array(
		'label'		=> esc_html__( 'Upload Sticky Logo Image', 'webnus_framework' ),
		'settings'	=> 'sticky_logo_image',
		'section'	=> 'logo_settings',
	) ) );

	// Logo width
	$wp_customize->add_setting( 'logo_width', array(
		'default'	=> '280',
		'sanitize_callback' => 'webnus_sanitize_number',
	) );

	$wp_customize->add_control( 'logo_width', array(
		'label'		=> esc_html__( 'Logo width', 'webnus_framework' ),
		'type'		=> 'number',
		'section'	=> 'logo_settings',
	) );

	// Transparent header logo width
	$wp_customize->add_setting( 'transparent_logo_width', array(
		'default'	=> '280',
		'sanitize_callback' => 'webnus_sanitize_number',
	) );

	$wp_customize->add_control( 'transparent_logo_width', array(
		'label'		=> esc_html__( 'Transparent header logo width', 'webnus_framework' ),
		'type'		=> 'number',
		'section'	=> 'logo_settings',
	) );

	// Sticky header logo width
	$wp_customize->add_setting( 'sticky_logo_width', array(
		'default'	=> '60',
		'sanitize_callback' => 'webnus_sanitize_number',
	) );

	$wp_customize->add_control( 'sticky_logo_width', array(
		'label'		=> esc_html__( 'Sticky header logo width', 'webnus_framework' ),
		'type'		=> 'number',
		'section'	=> 'logo_settings',
	) );

	$wp_customize->add_setting( 'logo_description' );
	$wp_customize->add_control( new Webnus_Customize_Description_Control( $wp_customize, 'logo_description', array(
		'label'		=> wp_kses( __( '<span style="color: red; font-weight: bold;">Note: </span>if elements which are available both in customizar and theme options get a change in customizer they\'ll be deactivated in theme options and priority is with customizer.', 'webnus_framework' ), array( 'span' => array( 'style' => array() ) ) ),
		'settings'	=> 'logo_description',
		'section'	=> 'logo_settings',
	) ) );

// Colorskin
	$wp_customize->add_section( 'colorskin', array(
		'title'		=> esc_html__( 'Colorskin', 'webnus_framework' ),
		'priority'	=> 22,
		'description'=> esc_html__('To access more options please go to Appearance > Theme Options > Styling Options', 'webnus_framework' ),
	) );

	// Custom Colorskin
	$wp_customize->add_setting( 'enable_custom_colorskin', array(
		'sanitize_callback' => 'webnus_sanitize_checkbox',
		'priority'	=> 1,
	) );

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'enable_custom_colorskin', array(
		'label'		=> esc_html__( 'Enable Custom Color Skin?', 'webnus_framework' ),
		'type'      => 'checkbox',
		'description'=> esc_html__('To make this feature work, set predefined colorskin on none'),
		'settings'	=> 'enable_custom_colorskin',
		'section'	=> 'colorskin',
	) ) );
	
	// Custom Colorskin
	$wp_customize->add_setting( 'custom_colorskin', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'priority'	=> 2,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'custom_colorskin', array(
		'label'		=> esc_html__( 'Custom Colorskin', 'webnus_framework' ),
		'settings'	=> 'custom_colorskin',
		'section'	=> 'colorskin',
	) ) );

	// Predefined Colorskin
	$wp_customize->add_setting( 'predefined_colorskin', array(
		'default' => false,
		'sanitize_callback' => '',
		'priority'	=> 3,
	) );

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'predefined_colorskin', array(
		'label'		=> esc_html__( 'Predefined Colorskin', 'webnus_framework' ),
		// 'description'=> esc_html__('Default Color: #181818'),
		'type'           => 'radio',
		'choices'        => array(
			false   => esc_html__( 'None' ),
			'1'		=> esc_html__( 'Gold' ),
			'2'		=> esc_html__( 'Blue' ),
			'3'		=> esc_html__( 'Red' ),
			'4'		=> esc_html__( 'Yellow' ),
			'5'		=> esc_html__( 'Pink' ),
			'6'		=> esc_html__( 'Green' ),
			'7'		=> esc_html__( 'Orchid' ),
			'8'		=> esc_html__( 'Jade' ),
			'9'		=> esc_html__( 'SkyBlue' ),
			'10'	=> esc_html__( 'Orange' ),
			'11'	=> esc_html__( 'Teal' ),
			'12'	=> esc_html__( 'DarkBlue' ),
			'13'	=> esc_html__( 'CoralPink' ),
			'14'	=> esc_html__( 'Brown' ),
			'15'	=> esc_html__( 'GreenYellow ' ),
			'16'	=> esc_html__( 'SplashBlue ' ),
			'17'	=> esc_html__( 'Gray ' ),
			'18'	=> esc_html__( 'Cyan ' ),
			'19'	=> esc_html__( 'Vine ' ),
			'20'	=> esc_html__( 'SplashRed ' ),
		),
		'settings'	=> 'predefined_colorskin',
		'section'	=> 'colorskin',
	) ) );

	$wp_customize->add_setting( 'colorskin_description' );
	$wp_customize->add_control( new Webnus_Customize_Description_Control( $wp_customize, 'colorskin_description', array(
		'label'		=> wp_kses( __( '<span style="color: red; font-weight: bold;">Note: </span>if elements which are available both in customizar and theme options get a change in customizer they\'ll be deactivated in theme options and priority is with customizer.', 'webnus_framework' ), array( 'span' => array( 'style' => array() ) ) ),
		'settings'	=> 'colorskin_description',
		'section'	=> 'colorskin',
	) ) );

// Typography
	$wp_customize->add_section( 'typography', array(
		'title'		=> esc_html__( 'Typography', 'webnus_framework' ),
		'description'=> esc_html__('To access more options please go to Appearance > Theme Options > Typography', 'webnus_framework' ),
		'priority'	=> 23,
	) );

	// Predefined Heading Typography
	$wp_customize->add_setting( 'predefined_heading_typography', array(
		'default' => false,
		'sanitize_callback' => '',
		'priority'	=> 1,
	) );

	$wp_customize->add_control( 'predefined_heading_typography', array(
		'label'		=> esc_html__( 'Predefined Heading Typography', 'webnus_framework' ),
		'type'           => 'select',
		'choices'        => array(
			false   => esc_html__( 'None' ),
			' ptg_h_1'		=> esc_html__( 'Montserrat' ),
			' ptg_h_2'		=> esc_html__( 'Rufina' ),
			' ptg_h_3'		=> esc_html__( 'PT Serif' ),
			' ptg_h_4'		=> esc_html__( 'Raleway' ),
			' ptg_h_5'		=> esc_html__( 'Philosopher' ),
		),
		'section'	=> 'typography',
	) );

	// Predefined paragraph Typography
	$wp_customize->add_setting( 'predefined_paragraph_typography', array(
		'default' => false,
		'sanitize_callback' => '',
		'priority'	=> 1,
	) );

	$wp_customize->add_control( 'predefined_paragraph_typography', array(
		'label'		=> esc_html__( 'Predefined Paragraph Typography', 'webnus_framework' ),
		'type'           => 'select',
		'choices'        => array(
			false   => esc_html__( 'None' ),
			' ptg_p_1'		=> esc_html__( 'Open Sans' ),
			' ptg_p_2'		=> esc_html__( 'Muli' ),
			' ptg_p_3'		=> esc_html__( 'Noto Sans' ),
			' ptg_p_4'		=> esc_html__( 'Roboto' ),
			' ptg_p_5'		=> esc_html__( 'Lora' ),
		),
		'section'	=> 'typography',
	) );

	$wp_customize->add_setting( 'typography_description' );
	$wp_customize->add_control( new Webnus_Customize_Description_Control( $wp_customize, 'typography_description', array(
		'label'		=> wp_kses( __( '<span style="color: red; font-weight: bold;">Note: </span>if elements which are available both in customizar and theme options get a change in customizer they\'ll be deactivated in theme options and priority is with customizer.', 'webnus_framework' ), array( 'span' => array( 'style' => array() ) ) ),
		'settings'	=> 'typography_description',
		'section'	=> 'typography',
	) ) );


// Topbar Colors
	$wp_customize->add_section( 'topbar_colors', array(
		'title'		=> esc_html__( 'Colors: Topbar', 'webnus_framework' ),
		'priority'	=> 24,
	) );
	
	// Topbar Background Color
	$wp_customize->add_setting( 'topbar_bg_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'priority'	=> 1,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'topbar_bg', array(
		'label'		=> esc_html__( 'Background', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #181818'),
		'section'	=> 'topbar_colors',
		'settings'	=> 'topbar_bg_color',
	) ) );

	// Search Background Color
	$wp_customize->add_setting( 'topbar_search_bg_color', array(
		'priority'	=> 2,
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'topbar_search', array(
		'label'		=> esc_html__( 'Search Background Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #4a4a4a'),
		'section'	=> 'topbar_colors',
		'settings'	=> 'topbar_search_bg_color',
	) ) );

	// Search Hover Background Color
	$wp_customize->add_setting( 'topbar_search_hover_bg_color', array(
		'priority'	=> 3,
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'topbar_search_hover', array(
		'label'		=> esc_html__( 'Search Hover Background Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #f9f9f9'),
		'section'	=> 'topbar_colors',
		'settings'	=> 'topbar_search_hover_bg_color',
	) ) );

	// Search Magnify Color
	$wp_customize->add_setting( 'topbar_search_magnify_color', array(
		'priority'	=> 4,
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'topbar_search_magnify', array(
		'label'		=> esc_html__( 'Search Magnify Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #bbbbbb'),
		'section'	=> 'topbar_colors',
		'settings'	=> 'topbar_search_magnify_color',
	) ) );

	// Contact Background Color
	$wp_customize->add_setting( 'topbar_contact_bg_color', array(
		'priority'	=> 5,
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'topbar_contact', array(
		'label'		=> esc_html__( 'Contact Background Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #4a4a4a'),
		'section'	=> 'topbar_colors',
		'settings'	=> 'topbar_contact_bg_color',
	) ) );

	// Contact Hover Background Color
	$wp_customize->add_setting( 'topbar_contact_hover_bg_color', array(
		'priority'	=> 6,
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'topbar_contact_hover', array(
		'label'		=> esc_html__( 'Contact Hover Background Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #ffac98'),
		'section'	=> 'topbar_colors',
		'settings'	=> 'topbar_contact_hover_bg_color',
	) ) );

// Menu Colors
	$wp_customize->add_section( 'menu_colors', array(
		'title'		=> esc_html__( 'Colors: Menu', 'webnus_framework' ),
		'priority'	=> 25,
	) );
	
	// Menu Text Color
	$wp_customize->add_setting( 'menu_text_color', array(
		'priority'	=> 1,
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menu_text', array(
		'label'		=> esc_html__( 'Menu Text Color', 'webnus_framework' ),
		'section'	=> 'menu_colors',
		'settings'	=> 'menu_text_color',
	) ) );

	// Menu Text Hover Color
	$wp_customize->add_setting( 'menu_text_hover_color', array(
		'priority'	=> 2,
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menu_text_hover', array(
		'label'		=> esc_html__( 'Menu Text Hover Color', 'webnus_framework' ),
		'section'	=> 'menu_colors',
		'settings'	=> 'menu_text_hover_color',
	) ) );

	// Selected Menu Text Color
	$wp_customize->add_setting( 'selected_menu_text_color', array(
		'priority'	=> 3,
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'selected_menu_text', array(
		'label'		=> esc_html__( 'Selected Menu Text Color', 'webnus_framework' ),
		'section'	=> 'menu_colors',
		'settings'	=> 'selected_menu_text_color',
	) ) );

	// Selected Menu Border Color
	$wp_customize->add_setting( 'selected_menu_border_color', array(
		'priority'	=> 4,
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'selected_menu_border', array(
		'label'		=> esc_html__( 'Selected Menu Border Color', 'webnus_framework' ),
		'section'	=> 'menu_colors',
		'settings'	=> 'selected_menu_border_color',
	) ) );

	// Dropdown Menu Background Color
	$wp_customize->add_setting( 'dropdown_menu_bg_color', array(
		'priority'	=> 5,
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dropdown_menu_bg', array(
		'label'		=> esc_html__( 'Dropdown Menu Background Color', 'webnus_framework' ),
		'section'	=> 'menu_colors',
		'settings'	=> 'dropdown_menu_bg_color',
	) ) );

	// Dropdown Menu Text Color
	$wp_customize->add_setting( 'dropdown_menu_text_color', array(
		'priority'	=> 6,
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dropdown_menu_text', array(
		'label'		=> esc_html__( 'Dropdown Menu Text Color', 'webnus_framework' ),
		'section'	=> 'menu_colors',
		'settings'	=> 'dropdown_menu_text_color',
	) ) );

	// Dropdown Menu Text Hover Color
	$wp_customize->add_setting( 'dropdown_menu_text_hover_color', array(
		'priority'	=> 7,
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dropdown_menu_text_h_color', array(
		'label'		=> esc_html__( 'Dropdown Menu Text Hover Color', 'webnus_framework' ),
		'section'	=> 'menu_colors',
		'settings'	=> 'dropdown_menu_text_hover_color',
	) ) );
	
	// Dropdown Menu Text Hover Background
	$wp_customize->add_setting( 'dropdown_menu_text_hover_bg', array(
		'priority'	=> 8,
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dropdown_menu_text_h_bg', array(
		'label'		=> esc_html__( 'Dropdown Menu Text Hover Background', 'webnus_framework' ),
		'section'	=> 'menu_colors',
		'settings'	=> 'dropdown_menu_text_hover_bg',
	) ) );

// Sidebar Colors
	$wp_customize->add_section( 'sidebar_colors', array(
		'title'		=> esc_html__( 'Colors: Sidebar', 'webnus_framework' ),
		'priority'	=> 26,
	) );

	// Sidebar Text Color
	$wp_customize->add_setting( 'sidebar_title_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'priority'	=> 1,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'widget_color', array(
		'label'		=> esc_html__( 'Widget Title Text Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #000'),
		'section'	=> 'sidebar_colors',
		'settings'	=> 'sidebar_title_color',
	) ) );

	// Sidebar Background Color
	$wp_customize->add_setting( 'sidebar_title_border_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'priority'	=> 2,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'widget_bg', array(
		'label'		=> esc_html__( 'Widget Title Border Bottom Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #D0AE5E'),
		'section'	=> 'sidebar_colors',
		'settings'	=> 'sidebar_title_border_color',
	) ) );

// Footer Colors
	$wp_customize->add_section( 'footer_colors', array(
		'title'		=> esc_html__( 'Colors: Footer', 'webnus_framework' ),
		'priority'	=> 27,
	) );
	
	// Footer Social Bar Background Color
	$wp_customize->add_setting( 'footer_social_bar_bg', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'priority'	=> 1,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_social_bg', array(
		'label'		=> esc_html__( 'Footer Social Bar Background Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #181818'),
		'section'	=> 'footer_colors',
		'settings'	=> 'footer_social_bar_bg',
	) ) );

	// Footer Background Color
	$wp_customize->add_setting( 'footer_bg_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'priority'	=> 1,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_bg', array(
		'label'		=> esc_html__( 'Footer Background Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #181818'),
		'section'	=> 'footer_colors',
		'settings'	=> 'footer_bg_color',
	) ) );

// Posts Colors
	$wp_customize->add_section( 'posts_colors', array(
		'title'		=> esc_html__( 'Colors: Posts', 'webnus_framework' ),
		'priority'	=> 28,
	) );
	
	// Posts Category Color
	$wp_customize->add_setting( 'post_cat_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'priority'	=> 1,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'post_cat', array(
		'label'		=> esc_html__( 'Post Category Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #ffac98'),
		'section'	=> 'posts_colors',
		'settings'	=> 'post_cat_color',
	) ) );

	// Posts Title Hover Color
	$wp_customize->add_setting( 'post_title_hover_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'priority'	=> 2,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'post_title', array(
		'label'		=> esc_html__( 'Post Title Hover Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #ffac98'),
		'section'	=> 'posts_colors',
		'settings'	=> 'post_title_hover_color',
	) ) );

	// Readmore Border Color
	$wp_customize->add_setting( 'readmore_border_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'priority'	=> 3,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'readmore_border', array(
		'label'		=> esc_html__( 'Readmore Border Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #ffac98'),
		'section'	=> 'posts_colors',
		'settings'	=> 'readmore_border_color',
	) ) );

	// Readmore Background Hover Color
	$wp_customize->add_setting( 'readmore_bg_hover_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'priority'	=> 4,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'readmore_bg', array(
		'label'		=> esc_html__( 'Readmore Background Hover Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #ffac98'),
		'section'	=> 'posts_colors',
		'settings'	=> 'readmore_bg_hover_color',
	) ) );

	// Post Author Color
	$wp_customize->add_setting( 'post_author_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'priority'	=> 5,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'author_color', array(
		'label'		=> esc_html__( 'Post Author Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #ffac98'),
		'section'	=> 'posts_colors',
		'settings'	=> 'post_author_color',
	) ) );

	// Post Social Background Color
	$wp_customize->add_setting( 'post_social_bg_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'priority'	=> 6,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'post_social_color', array(
		'label'		=> esc_html__( 'Post Social Background Color', 'webnus_framework' ),
		'description'=> esc_html__('Default Color: #ffac98'),
		'section'	=> 'posts_colors',
		'settings'	=> 'post_social_bg_color',
	) ) );

}
add_action( 'customize_register', 'webnus_customize_register' );

// Sanitize number options
if ( ! function_exists( 'webnus_sanitize_number' ) ) :
function webnus_sanitize_number( $value ) {
	return ( is_numeric( $value ) ) ? $value : intval( $value );
}
endif;

// Sanitize checkbox options
if ( ! function_exists( 'webnus_sanitize_checkbox' ) ) :
function webnus_sanitize_checkbox( $checked ) {
	return ( ( isset( $checked ) && ( true == $checked || 'on' == $checked ) ) ? true : false );
}
endif;