<?php function webnus_customizer_css() { ?>

	<style type="text/css">

		/* 1.0 - Topbar Colors Section
		–––––––––––––––––––––––––––––––––––––––––––––––––– */
		#wrap .top-bar { background: <?php echo get_theme_mod( 'topbar_bg_color' ); ?>; }
		#wrap #topbar-search .search-text-box { background-color: <?php echo get_theme_mod( 'topbar_search_bg_color' ); ?>; }
		#wrap #topbar-search:hover .search-text-box, #topbar-search .search-text-box:focus { background-color: <?php echo get_theme_mod( 'topbar_search_hover_bg_color' ); ?>; }
		#wrap .top-bar .inlinelb.topbar-contact { background-color: <?php echo get_theme_mod( 'topbar_contact_bg_color' ); ?>; }
		#wrap .top-bar .inlinelb.topbar-contact:hover { background-color: <?php echo get_theme_mod( 'topbar_contact_hover_bg_color' ); ?>; }
		#wrap #topbar-search .search-icon { color: <?php echo get_theme_mod( 'topbar_search_magnify_color' ); ?>; }
		
		/* 2.0 - Menu Colors Section
		–––––––––––––––––––––––––––––––––––––––––––––––––– */
		#wrap .nav-wrap2 #nav a { color: <?php echo get_theme_mod( 'menu_text_color' ); ?>; }
		#wrap .nav-wrap2 #nav > li:hover > a, #wrap .top-links #nav > li:hover > a { color: <?php echo get_theme_mod( 'menu_text_hover_color' ); ?>; }
		#wrap #nav > li.current > a, #wrap #nav > li > a.active { color: <?php echo get_theme_mod( 'selected_menu_text_color' ); ?>; }
		#wrap #nav > li.current > a:before { background: <?php echo get_theme_mod( 'selected_menu_border_color' ); ?>; }
		
		/* 3.0 - Sidebar Colors Section
		–––––––––––––––––––––––––––––––––––––––––––––––––– */
		#wrap .widget h4.subtitle { color: <?php echo get_theme_mod( 'sidebar_title_color' ); ?>; }
		#wrap .widget h4.subtitle:after { border-bottom-color: <?php echo get_theme_mod( 'sidebar_title_border_color' ); ?>; }

		/* 4.0 - Footer Colors Section
		–––––––––––––––––––––––––––––––––––––––––––––––––– */
		#wrap #pre-footer .footer-social-bar { background: <?php echo get_theme_mod( 'footer_social_bar_bg' ); ?>; }
		#wrap #footer { background: <?php echo get_theme_mod( 'footer_bg_color' ); ?>; }

		/* 5.0 - Post Colors Section
		–––––––––––––––––––––––––––––––––––––––––––––––––– */
		#wrap h6.blog-cat a { color: <?php echo get_theme_mod( 'post_cat_color' ); ?>; }
		#wrap .blog-post a:hover { color: <?php echo get_theme_mod( 'post_title_hover_color' ); ?>; }
		#wrap a.readmore { border-color: <?php echo get_theme_mod( 'readmore_border_color' ); ?>; }
		#wrap a.readmore:after { background: <?php echo get_theme_mod( 'readmore_bg_hover_color' ); ?>; }
		#wrap a.readmore:hover { background: <?php echo get_theme_mod( 'readmore_bg_hover_color' ); ?>; }
		#wrap h6.blog-author a { color: <?php echo get_theme_mod( 'post_author_color' ); ?>; }
		#wrap .blog-social a:hover { background: <?php echo get_theme_mod( 'post_social_bg_color' ); ?>; }
		
		/* Media Queries
		–––––––––––––––––––––––––––––––––––––––––––––––––– */
		@media only screen and (min-width: 961px) {
		/*Menu Colors Section*/
		#wrap .dark-submenu #nav ul, #wrap .dark-submenu #nav ul li, #wrap .dark-submenu #nav ul li:hover { background: <?php echo get_theme_mod( 'dropdown_menu_bg_color' ); ?>; }
		#wrap .dark-submenu #nav ul li a { color: <?php echo get_theme_mod( 'dropdown_menu_text_color' ); ?>; }
		#wrap #nav ul li a:hover, #wrap #nav li.current ul li a:hover, #wrap .nav-wrap2 #nav ul li a:hover, #wrap .nav-wrap2.darknavi #nav ul li a:hover, #wrap #nav ul li.current > a, #wrap #nav ul li:hover > a { color: <?php echo get_theme_mod( 'dropdown_menu_text_hover_color' ); ?>; }
		#wrap .dark-submenu #nav ul li a:hover { background: <?php echo get_theme_mod( 'dropdown_menu_text_hover_bg' ); ?>; }
		}

	</style>

<?php }
add_action( 'wp_head', 'webnus_customizer_css' );